//=====================================
//  DECLARATION VARIABLES
//=====================================

var gulp = require('gulp');
var csso = require('gulp-csso');
var less = require('gulp-less');
var concat = require("gulp-concat");
var path = "assets/site/";


//=====================================
//  COMPILE CSS VENDOR
//=====================================

//items to be compiled
var cssVendor = [
    'bower_components/bootstrap/dist/css/bootstrap.min.css',
    'bower_components/sweetalert2/dist/sweetalert2.min.css',
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/slick-carousel/slick/slick.css',
    'bower_components/normalize/normalize.css',
    'bower_components/lightgallery/dist/css/lightgallery.css',
    'bower_components/jquery-fileupload/dist/css/jquery.fileupload.css',
    'bower_components/jquery-fileupload/dist/css/jquery.fileupload-ui.css'
]

//function to minify and merge files
gulp.task('cssVendor', function () {
    gulp.src(cssVendor)
        .pipe(csso())
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(path + 'css'));
});

//=====================================
//  FONTES
//=====================================

//path fonts files
var fontsVendor = [
    'bower_components/bootstrap/dist/fonts/*',
    'bower_components/font-awesome/fonts/*'
]

//move fonts files to folder assets/fonts
gulp.task('fontsVendor', function () {
    gulp.src(fontsVendor)
        .pipe(gulp.dest(path + 'fonts'));
});


//=====================================
//  COMPILE JS VENDOR
//=====================================

// items to be compiled
var jsVendor = [
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/sweetalert2/dist/sweetalert2.min.js',
    'bower_components/jquery-mask-plugin/dist/jquery.mask.min.js',
    'bower_components/jquery-validation/dist/jquery.validate.min.js',
    'bower_components/slick-carousel/slick/slick.min.js',
    'bower_components/lightgallery/dist/js/lightgallery.min.js',
    'bower_components/moment/moment.js',
    'bower_components/jquery-fileupload/dist/js/vendor/jquery.ui.widget.js',
    'bower_components/jquery-fileupload/dist/js/jquery.iframe-transport.js',
    'bower_components/jquery-fileupload/dist/js/jquery.fileupload.js',
]

//function that joins the files
gulp.task('jsVendor', function () {
    gulp.src(jsVendor)
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(path + 'js/vendor'));
});


//=====================================
//  COMPILE LESS APPLICATION
//=====================================

//items to be compiled
var lessApp = [
    path + 'less/app/main.less',
]

//function to minify and merge files
gulp.task('less', function () {
    gulp.src(lessApp)
        .pipe(less())
        .pipe(concat('main.min.css'))
        .pipe(csso())
        .pipe(gulp.dest(path + 'css'));
});

//function waiting for change in less files
gulp.task('watch', ['less'], function () {
    gulp.watch([ path + 'less/*/*.less', path + 'less/*/*/*.less' ], ['less']);
});

//=====================================
//  DEFAULT
//=====================================

//function default of gulp
gulp.task('default' , ['watch'] );


//=====================================
//  VENDOR ASSETS
//=====================================

//create new assets
gulp.task('vendor', ['jsVendor','fontsVendor','cssVendor']);