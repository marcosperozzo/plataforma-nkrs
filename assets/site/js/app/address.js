"use strict";

function Address() {
    this.init();
};

Address.prototype.init = function(){
    var self = this;
    self.getCities();
    self.getAddressByCep();
    self.deleteAddress();
    $('[name="zipcode"]').mask('00000-000');
};

Address.prototype.deleteAddress = function(){
    $('[data-remove]').on('click', function(e){
        e.preventDefault();
        let btn = $(this);

        swal({
            title: 'Você quer excluir esse registro?',
            text: "Você não poderá reverter esse registro ao estado anterior!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, exclua isso!',
            cancelButtonText: 'Não'
        }).then(()=> {
            $.ajax({
                url: btn.data('url'),
                method: 'POST',
                data: {id: btn.data('remove')},
                success: function(response){
                    response = JSON.parse(response);
                    if (response.status) {
                        swal({ title: "Registro excluído!", text: response.message, type: "success" });
                        setTimeout(()=>{window.location.reload()},3000);
                    } else {
                        swal({ title: "Ops!", text: response.message, type: "warning" });
                    }
                },
                error: function(response){
                    console.log(response);
                    swal({ title: "Erro!", text: "Houve um erro ao conectar aos serviços.", type: "error" });
                }
            });
        });
    })
}

Address.prototype.getAddressByCep = function(){
    $('[name="zipcode"]').on('input', function(){
         //Nova variável "cep" somente com dígitos.
         var cep = $(this).val().replace(/\D/g, '');

         //Verifica se campo cep possui valor informado.
         if (cep != "") {

             //Expressão regular para validar o CEP.
             var validacep = /^[0-9]{8}$/;

             //Valida o formato do CEP.
             if(validacep.test(cep)) {

                 //Preenche os campos com "..." enquanto consulta webservice.
                 $('[name="street"]').val("...");
                 $('[name="suburb"]').val("...");
                 $('[name="aux-city"]').val("...");

                 //Consulta o webservice viacep.com.br/
                 $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                     if (!("erro" in dados)) {
                         //Atualiza os campos com os valores da consulta.
                         $('[name="street"]').val(dados.logradouro);
                         $('[name="suburb"]').val(dados.bairro);
                         $('[name="state"] option[data-uf="'+dados.uf+'"]').attr('selected','selected').trigger('change');
                         setTimeout(()=>{
                             $('[name="aux-city"]').val(dados.localidade).trigger('input');
                         },1000);
                     } //end if.
                     else {
                         //CEP pesquisado não foi encontrado.
                         swal({title: "Ops!", text: "CEP não encontrado.", type: "warning"});
                     }
                 });
             } //end if.
             else {
                //cep é inválido.
                if(cep.length == 8)
                    swal({title: "Ops!", text: "Formato de CEP inválido.", type: "warning"});
             }
         } //end if.
    });
}

Address.prototype.getCities = function(){
    $('[name="state"]').on('change', function(){
        let state = $(this);
        $.ajax({
            url: state.data('url'),
            method: 'POST',
            data: {state: state.val()},
            success: function(response){
                response = JSON.parse(response);
                if(!!response.status){
                    $('#cities').html('');
                    response.data.forEach(city => {
                        let template = `<option data-value="${city.id}">${city.name}</option>`;
                        $('#cities').append(template);
                    });
                } else {
                    swal({ title: "Erro!", text: "Não foi possível carregar a lista de cidades." , type: "error"});
                }
            },
            error: function(response){
                swal({ title: "Erro!", text: response.message , type: "error"});
            }
        });
    });
    $('[name="aux-city"]').on('input', function(){
        let city = $(this).val(),
            value = $('#cities').find('option:contains('+city+')').data('value');
        $('[name="city"]').val(value);
    })
}

$(document).ready(function(){
    new Address();
});
