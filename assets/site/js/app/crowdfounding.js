"use strict";

function Crowdfounding() {
    this.init();
};

Crowdfounding.prototype.init = function(){
    var self = this;
    self.triggerDelete();
};

Crowdfounding.prototype.delete = function(url, data){
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        success: function(response){
            response = JSON.parse(response);
            if (response.status) {
                swal({ title: "Registro excluído!", text: response.message, type: "success" });
                setTimeout(()=>{window.location.reload()},3000);
            } else {
                swal({ title: "Ops!", text: response.message, type: "warning" });
            }
        },
        error: function(response){
            console.log(response);
            swal({ title: "Erro!", text: "Houve um erro ao conectar aos serviços.", type: "error" });
        }
    });
}

Crowdfounding.prototype.triggerDelete = function(){
    var self = this;
    $('[data-remove]').on('click', function(e){
        e.preventDefault();
        let btn = $(this);

        swal({
            title: 'Você quer excluir esse registro?',
            text: "Você não poderá reverter esse registro ao estado anterior!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, exclua isso!',
            cancelButtonText: 'Não'
        }).then(()=> {
            self.delete(btn.data('url'),{id: btn.data('remove')})
        });
    });
};

$(document).ready(function(){
    new Crowdfounding();
});
