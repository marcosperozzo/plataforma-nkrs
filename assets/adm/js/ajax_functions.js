$(function () {
    'use strict';

    //////////////////////////////////
    //Ajax functions
    //////////////////////////////////

    //Triggers
	$("body").on("change","input.ajaxUpdate,select.ajaxUpdate",function(event){
		ajaxUpdate($(this),event);
	});

	$("body").on("blur","input.ajaxUpdateBlur",function(event){
		ajaxUpdate($(this),event);
	});

	$("body").on("change","input.cepUpdate",function(event){
		cepUpdate($(this),event);
	});
});

//Clear data of input, select
function clearField(fieldsName) {
	if (fieldsName instanceof Array) {
		$.each( fieldsName, function( key, value ) {
			if ($("[name='"+value+"']").prop('nodeName')=="SELECT") {
				$("[name='"+value+"']").empty();
			} else {
				$("[name='"+value+"']").val("");
			}
		});
	} else {
		if ($("[name='"+fieldsName+"']").prop('nodeName')=="SELECT") {
			$("[name='"+fieldsName+"']").empty();
		} else {
			$("[name='"+fieldsName+"']").val("");
		}
	}
}

//Run the ajaxUpdate fields
function ajaxUpdate(element,event) {
	var dfd = jQuery.Deferred();

	if (event) {
		event.preventDefault();
	}
	var customAlert={};
	var execute=true;
	var postData={};
	var token=$('body').data("token");
	var trigger=element;
	var inputName=trigger.attr("name");
	var inputValue=trigger.val();
	var target=trigger.data("target");
	var selectedValue=$("[name='"+target+"']").attr("data-to-select");
	var url=trigger.data("url");
	var showid=trigger.data("showid");
	var ignoreEmpty=trigger.data("ignore-empty");
	var ignoreEmptyFirstOption=$("[name='"+target+"']").data("ignore-empty-first-option");
	var checkEmpty=trigger.data("check-empty");

	if (typeof checkEmpty != 'undefined') {
		var checkInput=$("[name='"+checkEmpty+"']").val();
		if (checkInput!=null) {
			execute=false;
		}
	}

	if (execute==true) {
		if (inputValue!="") {
			if (trigger.attr('data-extra')) {
				var extra=trigger.data("extra").split(",");
			} else {
				var extra=[];
			}

			postData[inputName]=inputValue;

			$.each( extra, function( key, value ) {
				extraValue=$("[name='"+value+"']").val();
				postData[value]=extraValue;
			});

			$.ajax ({
				type: "POST",
				url: url,
				data: postData,
				headers: {
					"Authorization": "Bearer " + token
				},
				success: function (data) {
					if ($("[name='"+target+"']").prop('nodeName')=="SELECT") {
						$("[name='"+target+"']").empty().prop("disabled",false);
						if (typeof data[1].label != 'undefined') {
							$.each(data, function (index) {
								if (index==0) {
									if (selectedValue==data[index].id) {
										selectedOption=true;
										$("[name='"+target+"']").attr("data-to-select","");
									} else {
										selectedOption=false;
									}

									if (showid===true) {
										var optgroup = $("<option></option>", { 
											value: data[index].id,
											text : data[index].id+' - '+data[index].nome,
											selected: selectedOption
										});
									} else {
										var optgroup = $("<option></option>", { 
											value: data[index].id,
											text : data[index].nome,
											selected: selectedOption
										});
									}
								} else {
									var optgroup = $('<optgroup>');
									optgroup.attr('label',data[index].label);

									$.each(data[index].items, function (i) {
										if (selectedValue==data[index].items[i].id) {
											selectedOption=true;
											$("[name='"+target+"']").attr("data-to-select","");
										} else {
											selectedOption=false;
										}

										if (showid===true) {
											var option = $("<option></option>", { 
												value: data[index].items[i].id,
												text : data[index].items[i].id+' - '+data[index].items[i].nome,
												selected: selectedOption
											});
										} else {
											var option = $("<option></option>", { 
												value: data[index].items[i].id,
												text : data[index].items[i].nome,
												selected: selectedOption
											});
										}

										optgroup.append(option);
									});
								}

								$("[name='"+target+"']").append(optgroup);
							});
						} else if (typeof data[1].label == 'undefined') {
							if (ignoreEmptyFirstOption==true) {
								data.splice(0,1);
							}
							$.each(data, function (i, item) {
								if (selectedValue==item.id) {
									selectedOption=true;
									$("[name='"+target+"']").attr("data-to-select","");
								} else {
									selectedOption=false;
								}
								if (showid===true) {
									$("[name='"+target+"']").append($('<option>', { 
										value: item.id,
										text : item.id+' - '+item.nome,
										selected: selectedOption
									}));
								} else {
									$("[name='"+target+"']").append($('<option>', { 
										value: item.id,
										text : item.nome,
										selected: selectedOption
									}));
								}
							});
						} else {
							$("[name='"+target+"']").append($('<option>', { 
								text: "Não há itens disponíveis",
								value : "" 
							})).prop("disabled",true);
						}
						$("[name='"+target+"']").trigger("chosen:updated");
						if ($("[name='"+target+"']").hasClass("multiselect")) {
							$("[name='"+target+"']").multiSelect('refresh');
						}
					} else {
						$("[name='"+target+"']").val(data);
					}
					dfd.resolve(true);
				},
				error: function () {
					dfd.reject(false);
					customAlert.title="OOPS!";
					customAlert.message="Ocorreu um problema. Tente novamente mais tarde.";
					customAlert.type="error";
					swal(customAlert.title, customAlert.message, customAlert.type);
				}
			});
		} else if(inputValue=="" && ignoreEmpty==true) {
			$("[name='"+target+"']").val("");
			if ($("[name='"+target+"']").prop('nodeName')=="SELECT") {
				$("[name='"+target+"']").trigger("chosen:updated");
			}
			dfd.resolve(true);
		} else {
			dfd.reject(false);
			customAlert.title="OOPS!";
			customAlert.message="Você precisa preencher/escolher um valor.";
			customAlert.type="error";
			swal(customAlert.title, customAlert.message, customAlert.type);
		}
		return dfd.promise();
	}
}

//Run the cepUpdate function
function cepUpdate(element,event) {
	var state=element.data("state");
	var actualState=$("select[name='"+state+"']").val();
	var city=element.data("city");
	var address=element.data("address");
	var neighborhood=element.data("neighborhood");
	var ibge=element.data("ibge");
	var postalCode=element.val();
	var customAlert={};

	if(postalCode.length==8) {
		$("input[name='"+address+"']").prop("disabled",true);
		$("input[name='"+neighborhood+"']").prop("disabled",true);
		if ($("select[name='"+city+"']").length>0) {
			$("select[name='"+city+"']").prop("disabled",true);
		} else {
			$("input[name='"+city+"']").prop("disabled",true);
		}
		$("select[name='"+state+"']").prop("disabled",true);
		$("input[name='"+ibge+"']").prop("disabled",true);
		element.prop("disabled",true);

		$.getJSON("//viacep.com.br/ws/"+ postalCode +"/json/?callback=?", function(data) {
			if (!("erro" in data)) {
				$("input[name='"+address+"']").val(data.logradouro.toUpperCase());
				$("input[name='"+neighborhood+"']").val(data.bairro.toUpperCase());
				if (actualState==data.uf) {
					if ($("select[name='"+city+"']").length>0) {
						$("select[name='"+city+"']").val(data.localidade.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase()).trigger('change');
					} else {
						$("input[name='"+city+"']").val(data.localidade.toUpperCase()).trigger('change');
					}
				} else {
					if ($("select[name='"+city+"']").length>0) {
						$("select[name='"+city+"']").attr("data-to-select",data.localidade.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase());
					} else {
						$("input[name='"+city+"']").val(data.localidade.toUpperCase()).trigger('change');
					}
					$("select[name='"+state+"']").val(data.uf.toUpperCase()).trigger('change');
				}
				$("input[name='"+ibge+"']").val(data.ibge);
			} else {
				customAlert.title="HEY!";
				customAlert.message="Não encontramos os dados do CEP informado. Informe os mesmos manualmente.";
				customAlert.type="info";
				swal(customAlert.title, customAlert.message, customAlert.type);
			}
		})
		.error(function(){
			customAlert.title="HEY!";
			customAlert.message="Não encontramos os dados do CEP informado. Informe os mesmos manualmente.";
			customAlert.type="info";
			swal(customAlert.title, customAlert.message, customAlert.type);
		})
		.complete(function(){
			$("input[name='"+address+"']").prop("disabled",false);
			$("input[name='"+neighborhood+"']").prop("disabled",false);
			if ($("select[name='"+city+"']").length>0) {
				$("select[name='"+city+"']").prop("disabled",false);
			} else {
				$("input[name='"+city+"']").prop("disabled",false);
			}
			$("select[name='"+state+"']").prop("disabled",false);
			$("input[name='"+ibge+"']").prop("disabled",false);
			element.prop("disabled",false);
			$(".chosen").trigger("chosen:updated");
		});
	} else {
		customAlert.title="OOPS!";
		customAlert.message="O CEP informado não é válido.";
		customAlert.type="error";
		swal(customAlert.title, customAlert.message, customAlert.type);
	}
}