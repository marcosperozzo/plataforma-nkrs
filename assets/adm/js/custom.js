$(function () {
    'use strict';

    //////////////////////////////////
    //Mask Plugin
    //////////////////////////////////
	$('.datebr').mask('00/00/0000', {placeholder: "__/__/____"});
	$('.cep').mask('00000-000');
	$('.cpf').mask('000.000.000-00');
	$('.time').mask('00:00');
	$('.onlyNumber').mask('000000000000000');
	$('.hour').mask('H0:M0', {
		translation: {
			'H': {
				pattern: /[0-2]/
			},
			'M': {
				pattern: /[0-5]/
			}
		}
	});

	//Phone Mask
	var maskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
	options = { onKeyPress: function(val, e, field, options) {
			field.mask(maskBehavior.apply({}, arguments), options);
		}
	};
	$('.phone').mask(maskBehavior, options);


    //////////////////////////////////
    //Enable Plugins
    //////////////////////////////////

    //Enable Chosen Plugin
	if ($('.chosen').length) {
		$('.chosen').chosen({
			width: '100%',
			no_results_text: "OPS! Nenhum resultado encontrado",
		}).on('change', function(evt, params) {
			var id = $(this).attr("id");
			if (params.selected!='') {
				$("#"+id).removeClass("error");
				$("#"+id+"_chosen a").removeClass("error");
				$("#"+id+"-error").remove();
			}
		});
	};

    //Enable Sortable List
	if ($('.sortable-list').length>0) {
		$('.sortable-list').sortable({
			placeholder: 'ui-file-placeholder col-md-3 opct-50',
			opacity: 0.5,
			update: function( event, ui ) {
				var block=$(this).closest('.upload-block');
				var type=block.data("type");
				var sorted=$(this).sortable( "toArray", { attribute: "data-id" } );
				var token=$('body').data("token");

				//Execute ajax
				$.ajax({
					url : baseURL+"adm/attachments/order",
					type: "POST",
					data : {items:sorted},
					dataType: "json",
					cache: false,
					headers: {
					  "Authorization": "Bearer " + token
					},
					error: function (jqXHR, textStatus, errorThrown) {
					  var data=jqXHR.responseJSON;
					  if (typeof data =='object') {
					    if (data.status=="error" && data.message!="") {
					      swal("OOPS!", data.message, data.status);
					    } else {
					      swal("OOPS!", "Ocorreu um erro no sistema. Tente novamente mais tarde.", "error");
					    }
					  } else {
					    swal("OOPS!", "Ocorreu um erro no sistema. Tente novamente mais tarde.", "error");
					  }
					}
				});
			}
		}).disableSelection();
	}

	//Enable Tooltips on the elements
	if ($('[data-toggle=tooltip]').length>0) {
		$("[data-toggle=tooltip]").tooltip();
	}

	//Enable Lazy Load on images
	if ($('img.lazy').length>0) {
		$("img.lazy").lazyload();
	}

	//Enable the Gallery Plugin
	if ($('.gallery').length>0) {
		$('.gallery').Chocolat({
			loop: true,
			imageSize: 'cover',
			overlayOpacity: 1.0,
			duration: 150
		});
	};

	//Enable the DatePicker Plugin
	if ($('.datebr').length>0) {
		$('.datebr').datepicker({
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});
	};

	//Enable the DataTable Plugin
	var dataTable="";
	if ($('.datatable').length>0) {
		//Extend the plugin to sort dates right
		$.fn.dataTable.moment = function ( format, locale="pt-br" ) {
			var types = $.fn.dataTable.ext.type;

			// Add type detection
			types.detect.unshift( function ( d ) {
				d=$(d).text();
				return moment( d, format, locale, true ).isValid() ?
				'moment-'+format :
				null;
			} );

			// Add sorting method - use an integer for the sorting
			types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
				d=$(d).text();
				return moment( d, format, locale, true ).unix();
			};
		};

		$.fn.dataTable.moment('DD/MM/YYYY');

		var tableOrder="asc";
		if ($(".datatable").hasClass("desc")) {
			tableOrder="desc";
		}

		dataTable = $('.datatable').DataTable( {
			"info": false,
			"stateSave": false,
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
			},
			"columnDefs": [
				{"targets": 'nosort', "orderable": false }
			],
			"order": [[ 1, tableOrder ]]
		});
	};

	//Enable editor plugin
	if ($('.editor').length>0) {

		// FUNÇÃO DE NOVO BOTÃO
		var btnGalleryFunc = function (context) {
			var ui = $.summernote.ui;

			var galleryOptions = [];

			// PERCORRE AS LISTAS DE UPLOADS EXISTENTES E MONTA A ESTRUTURA COM O NOME DAS GALERIAS
			$.each($('[data-parentblock]'), function (index, el) { 
				var text = $(this).closest('.form-group').find('.control-label').text().replace('Enviar ', '');
				text = "<li data-tag='#gallery-"+$(el).data('parentblock')+"'>Galeria "+text+"</li>";
				galleryOptions.push(text);
			});

			// CRIA E RENDERIZA O BOTÃO
			var button = ui.buttonGroup([
				ui.button({
					className: 'dropdown-toggle toogleGallery',
					contents: '<i class="fa fa-object-ungroup"></i> <span class="caret"></span>',
					tooltip: "Insira uma Galeria",
					data: {
						toggle: 'dropdown'
					}
				}),
				ui.dropdown({
					className: 'dropdownCustom dropdown-gallery',
					contents: "<ul>"+galleryOptions.toString().replace(/\,/g, '')+"</ul>",
					callback: function ($dropdown) {
						$dropdown.find('li').each(function () {
							$(this).click(function() {
										context.invoke("editor.insertText", $(this).data('tag'));
							});
						});
					}
				})
			]);
			if ($('.upload-block').length>0) {
				return button.render();   // return button as jquery object
			}
		}

		// FUNÇÃO DE NOVO BOTÃO
		var btnImageFunc = function (context) {

			var ui = $.summernote.ui;

			var imageOption = [];
			var titleOld;

			// PERCORRE TODAS AS IMAGENS UPADAS E MONTA A ESTRUTURA COM A THUMB E O NOME DAS IMAGENS
			$.each($('.files li'), function (index, el) { 
				
				var urlImage = $(el).find('.thumbnail img').attr('src');
				var valInput = $(el).find('.attach-desc').val();
				var id = $(el).attr('id');
				var title = $(el).closest('.form-group').find('.control-label').text().replace('Enviar ', '');

				// VERIFICA SE O TITULO DA JA FOI UTILIZADO, SE JA FOR EXISTENTE ELE NÃO COLOCA O TITULO NOVAMENTE
				if (title !== titleOld) {
					var text = "<h3>Galeria "+title+"</h3><li data-id='#"+id+"'><img class='img img-responsive' src='"+urlImage+"' width='40px' />"+valInput+"</li>";
					titleOld = title;
				} else{
					var text = "<li data-id='#"+id+"'><img class='img img-responsive' src='"+urlImage+"' width='40px' />"+valInput+"</li>";
				}
				
				imageOption.push(text);
				
			});

			// ATUALIZA A LISTA DE IMAGENS
			$('.dropdownImage ul').html(imageOption.toString().replace(/\,/g, ''));

			// CLICK DAS IMAGENS PARA PARA INSERIR ELA NO BLOCO
			$.each($('.dropdownImage ul li'), function () { 
				
				 $(this).click(function(){
					$('.editor').summernote('insertText', $(this).data('id'));
				 });
			});

			// create button
			var button = ui.buttonGroup([
				ui.button({
					className: 'dropdown-toggle toogleImage',
					contents: '<i class="fa fa-picture-o"></i> <span class="caret"></span>',
					tooltip: "Insira uma Imagem",
					data: {
						toggle: 'dropdown'
					}
				}),
				ui.dropdown({
					className: 'dropdownCustom dropdownImage',
					contents: "<ul>"+imageOption.toString().replace(/\,/g, '')+"</ul>",
					callback: function ($dropdown) {
						$dropdown.find('li').each(function () {
							$(this).click(function() {
								context.invoke("editor.insertText", $(this).data('id'));
							});
						});
					}
				})
			]);
			if ($('.upload-block').length>0) {
				return button.render();   // return button as jquery object
			}
		}

		// EXECUTA E MONTA NOVAMENTE O BOTÃO DAS IMAGENS
		$(document).on('click', '.toogleImage', function(e){
			e.preventDefault();
			btnImageFunc();
		})

		$('.editor').summernote({
		  height: 300,
			lang: 'pt-BR',
			disableDragAndDrop: true,
		  toolbar: [
		  // [groupName, [list of button]]
		    ['style', ['bold', 'italic', 'underline', 'clear']],
		    ['font', ['strikethrough', 'superscript', 'subscript']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['insert', ['link','unlink','table']],
				['adv',['codeview']],
				['mybutton', ['btnGallery', 'btnImage']]
			],
			buttons: {
				btnGallery: btnGalleryFunc,
				btnImage: btnImageFunc
			},
			callbacks : {
				onPaste: function (e) {
					var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
					e.preventDefault();
					$(this).summernote('editor.insertText', bufferText);
				}
			}
		});
		
	}

    //////////////////////////////////
    //General Functions
    //////////////////////////////////

    //Function to show Sweet Alert
	function showAlert(alertData) {
		swal(alertData.title, alertData.message, alertData.type);
	}

    //Function to enable or disable an element
    function enableDisableElement(relationObject,targetId) {
		if (relationObject.length) {
			$("#"+targetId).prop("disabled",false);
		} else {
			$("#"+targetId).prop("disabled",true);
		}
    }

	//Open and close the elements to edit content inline
	$('.edit-inline, .close-edit-inline').on("click", function() {
		var parent=$(this).closest('.edit-parent');
		var variable=parent.data("edit");
		var original=parent.find('.edit-enabled');
		var editInput=parent.find('.edit-disabled');
		$(original).removeClass('edit-enabled').addClass('edit-disabled');
		$(editInput).addClass('edit-enabled').removeClass('edit-disabled');
	});

	//Add and remove blocks of content
	$('button.addRow').on("click",function(event){
		var target=$(this).data('target');
		var markup_id=$(this).data('markup');

		var dataReplace={};
		dataReplace.NIDX=Math.floor((Math.random() * 10000000000) + 1);
		dataReplace.form="form";

		var markupData = Handlebars.compile($("#"+markup_id).html());
		var template=markupData(dataReplace);
        $(template).appendTo("#"+target);
        fileUpload();
	});

	$('form.form-horizontal').on("click", "button.removeThisRow",function(event){
		$(this).closest('.rowParent').remove();
	});

	//Delete button on lists (Datatable)
	$("#deleteBtn").on("click", function(){
		var $itens = $('input[name="delete[]"]:checked');
		var actionUrl = $(this).data('url');
		var customAlert={};

		if ($itens.length>0) {
		  swal({
		    title: "Você tem certeza?",
		    text: "Ao excluir estes registros, não será possível recuperar eles nem suas dependências.",
		    type: "warning",
		    showCancelButton: true,
		    confirmButtonColor: "#dd6777",
		    confirmButtonText: "Sim, quero excluir!",
		    closeOnConfirm: false,
		    cancelButtonText: "Cancelar"
		  },
		  function(){
		    $itens.each(function() {
		      var token=$('body').data("token");
		      var item=$(this);

		      $.ajax({
		        url : actionUrl+item.val()+"/",
		        type: "POST",
		        dataType: "json",
		        headers: {
		          "Authorization": "Bearer " + token
		        },
		        success: function(data, textStatus, jqXHR) {
		          if (data.status=="ok") {
		            item.closest("tr").addClass("removeRow");
		            dataTable.row('.removeRow').remove().draw(false);
		          } else {
		            customAlert.title="OOPS!";
		            customAlert.message=data.message;
		            customAlert.type="error";
		            showAlert(customAlert);
		            return false;
		          }
		        },
		        error: function (jqXHR, textStatus, errorThrown) {
		          customAlert.title="OOPS!";
		          customAlert.type="error";

		          var data=jqXHR.responseJSON;
		          if (data.status=="error" && data.message!="") {
		            customAlert.message=data.message;
		          } else {
		            customAlert.message="Ocorreu um erro no sistema. Tente novamente mais tarde.";
		          }
		          showAlert(customAlert);
		          return false;
		        }
		      });
		    });
		    customAlert.title="YEAH!";
		    customAlert.message="Registros excluídos com sucesso.";
		    customAlert.type="success";
		    showAlert(customAlert);
		  });      
		} else {
			customAlert.title="OOPS!";
			customAlert.message="Selecione algum registro para excluir.";
			customAlert.type="error";
		}
		swal(customAlert.title, customAlert.message, customAlert.type);
	});

	//Save the form
	$('form').on("click", "button#saveBtn",function(event){
		var thisForm=$(this).closest('form');
		var thisButton=$(this);

		//Get the original values and icons of the send button
		var originalBtnIconArray=thisButton.find('i').attr("class").split(" ");
		for (var i = originalBtnIconArray.length - 1; i >= 0; i--) {
		  if (originalBtnIconArray[i]!="mr5") {
		    var originalBtnIcon=originalBtnIconArray[i];
		  }
		}
		var originalBtnText=thisButton.find('span').text();

		var $vldtSave=$(thisForm).validate({
		  lang: 'pt-br',
		  ignore:':hidden:not(.chosen,.editor)',
		  errorPlacement: function(error, element) {
		    if(element.hasClass('chosen')) {
		      var id = element.attr("id");
		      $("#"+id+"_chosen a").addClass("error");
		      error.insertAfter("#"+id+"_chosen");
		    } else if(element.hasClass('editor')) {
		      $(".note-editor").addClass("error");
		      error.insertAfter(".note-editor");
		    } else {
		      error.insertAfter(element);
		    }
		  },
		  onfocusout: function (element) {
		    if($(element).hasClass('chosen')) {
		      var id = element.attr("id");
		      $("#"+id+"_chosen a").removeClass("error");
		    } else if($(element).hasClass('editor')) {
		      $(".note-editor").removeClass("error");
		    }
		  }
		});

		var $validSave = $(thisForm).valid();

		if (!$validSave) {
		  $vldtSave.focusInvalid();
		  return false;
		} else {
		  event.preventDefault();

		  thisButton.prop("disabled",true);
		  thisButton.find('i').removeClass('icon-cursor').addClass('fa fa-hourglass-half');
		  thisButton.find('span').text("Enviando");

		  var moduleName=thisForm.data('module');
		  var idValue=$('input[name="id"]').val();
		  var actionUrl=thisForm.data('url');

		  var $inputs = $('form[data-module="'+moduleName+'"] :input:not([type=radio], [type=checkbox]), form[data-module="'+moduleName+'"] :input:checked');
		  var postData = {};
		  var mentions=[];
		  var contentType='application/x-www-form-urlencoded; charset=UTF-8';
		  var processData=true;
		  $inputs.each(function() {
		     if ($(this).hasClass("editor")) {
		     	var rawContent=$(this).val();
		     	var content=$.parseHTML(rawContent);
		     	var mentionsTags=$(content).find("span.mention");
		     	$(mentionsTags).each(function() {
			       	mentions.push($(this).data("id"));
		     	});
		     }
		     if (this.name.indexOf('[]') >= 0) {
		     	if (typeof postData[this.name] == 'undefined') {
			       	postData[this.name]=[];
		     	}
		     	postData[this.name].push($(this).val());
		     } else if (this.name.length>0 && this.name!="files") {
		     	postData[this.name] = $(this).val();
		     }
		  });
		  postData["mentions"]=JSON.stringify(mentions);

		  var token=$('body').data("token");

		  $.ajax({
		    url : actionUrl,
		    type: "POST",
		    data : postData,
		    dataType: "json",
		    cache: false,
		    contentType: contentType,
		    processData:processData,
		    headers: {
		      "Authorization": "Bearer " + token
		    },
		    success: function(data, textStatus, jqXHR) {
		      var customAlert={};
		      if (data.status=="ok") {
		        if (data.created_id!==undefined) {
		          $('input[name="id"]').val(data.created_id);
		          swal({
		            title: "YEAH!",
		            text: data.message,
		            type: "success",
		            showCancelButton: true,
		            confirmButtonColor: "#6fc080",
		            confirmButtonText: "Ir para a lista",
		            cancelButtonText: "Incluir outro registro",
		            closeOnConfirm: false,
		            closeOnCancel: true,
		          },
		          function(isConfirm){
		            if (isConfirm) {
		              var listLink=$("#backBtn").attr("href");
		              window.location=listLink;
		            } else {
						thisForm.trigger("reset");
						$('input[name="id"]').val('');
						if($('.editor').length > 1) {
							$('.editor').each(function(i, el){
								$(el).summernote('reset');
							});
						} else {
							$('.editor').summernote('code','');
						}
						$(".chosen").trigger('chosen:updated');
						$('.upload-block').each(function(index, el) {
							$("input[name='prefiles[]']").remove();
							$(this).find(".fileinput-button").show();
							$(this).find(".fileinput-alert").addClass('hidden');
							$(this).find(".files li").remove();

							var turnAvaliable=$(this).find(".fileupload").data("turnavaliable");
							var uploadedFiles=$(this).find(".files li");
							enableDisableElement(uploadedFiles,turnAvaliable);
						});
		            }
		          });
		        } else {
		          customAlert.title="YEAH!";
		          customAlert.message=data.message;
		          customAlert.type="success";
		          swal(customAlert.title, customAlert.message, customAlert.type);
		        }
		      } else if (data.status=="info") {
		        customAlert.title="HEY!";
		        customAlert.message=data.message;
		        customAlert.type="info";
		        swal(customAlert.title, customAlert.message, customAlert.type);
		      } else if (data.status=="redirect") {
		        window.location=data.url;
		      } else {
		        customAlert.title="OOPS!";
		        customAlert.message=data.message;
		        customAlert.type="error";
		        swal(customAlert.title, customAlert.message, customAlert.type);
		      }
		    },
		    error: function (jqXHR, textStatus, errorThrown) {
		      var data=jqXHR.responseJSON;
		      if (typeof data =='object') {
		        if (data.status=="error" && data.message!="") {
		          swal("OOPS!", data.message, data.status);
		        } else {
		          swal("OOPS!", "Ocorreu um erro no sistema. Tente novamente mais tarde.", "error");
		        }
		      } else {
		        swal("OOPS!", "Ocorreu um erro no sistema. Tente novamente mais tarde.", "error");
		      }
		    },
		    complete: function () {
		      thisButton.prop("disabled",false);
		      thisButton.find('i').removeClass('fa fa-hourglass-half').addClass(originalBtnIcon);
		      thisButton.find('span').text(originalBtnText);
		    }
		  });
		}
	});

    //////////////////////////////////
    //Ajax Upload Functions
    //////////////////////////////////

    //Style the drag area when draggin a file
    $('.fileinput-button').on('dragover',function(e){
		$(this).addClass('dragover');
    });
    $('.fileinput-button').on('dragleave',function(e){
    	$(this).removeClass('dragover');
    });

    //Upload the file
	var fileUpload = function() {
		$('.fileupload').each(function(index, el) {
			var functionUpload=$(this);
			var customAlert={};
            var maxFilesInit=$(this).data("maxfiles");
        	var blockInit=$(this).data("parentblock");
            var uploadedFilesInit=$("#"+blockInit).find(".files li");

            if (maxFilesInit>0 && uploadedFilesInit.length>=maxFilesInit) {
            	$("#"+blockInit+" .fileinput-button").hide();
            	$("#"+blockInit+" .fileinput-alert").removeClass('hidden');
            }

			var turnAvaliableInit = $(this).data("turnavaliable");
            if (turnAvaliableInit.length) {
	            enableDisableElement(uploadedFilesInit,turnAvaliableInit);
            }

		    $(functionUpload).fileupload({
		        url: $(this).data("url"),
		        dataType: 'json',
		        add: function (ev, returnData) {
		        	//When add a file to the queue
					var uploadErrors = [];
					var maxFiles=maxFilesInit;
					var block=blockInit;
					var totalSentFiles=returnData.originalFiles.length;
					var isDragDrop=false;

					if (ev.delegatedEvent.handleObj.type=="drop") {
						isDragDrop=true;
						var targetParent=$(ev.target).data("parentblock");
						var dragActiveBlock=$(".dragover").parent().attr("id");
						var uploadedFiles=$("#"+dragActiveBlock).find(".files li").length;
					} else {
						var uploadedFiles=$("#"+block).find(".files li").length;
					}

		            var arrayMineTypes=$(this).data("filetypes").split(",");
					var acceptFileTypes = new RegExp(arrayMineTypes.join("|"), "i");
		            if(returnData.originalFiles[0]['type'].length && !acceptFileTypes.test(returnData.originalFiles[0]['type'])) {
		                uploadErrors.push('O tipo de arquivo selecionado não é válido.');
		            }

		            if (maxFiles>0 && ((totalSentFiles+uploadedFiles)>maxFiles)) {
		            	uploadErrors.push('Você só pode enviar até '+maxFiles+' arquivo(s).')
		            }

					if(uploadErrors.length > 0) {
						customAlert.title="OOPS!";
						customAlert.message=uploadErrors.join("\n");
						customAlert.type="error";
						showAlert(customAlert);
		            } else {
		            	if ((isDragDrop && targetParent==dragActiveBlock) || isDragDrop==false) {
							$("#"+block+" .progress-bar").removeClass('hidden').show();
							returnData.process().done(function () {
								returnData.formData = $("#"+block+" .fileupload :input").serializeArray();
								returnData.submit();
							});
		            	}
		            }
		        },
		        fail: function (e, returnData) {
		        	var block=$(this).data("parentblock");
		            $("#"+block+" .progress-bar").fadeOut();
					customAlert.title="OOPS!";
					customAlert.message="Ocorreu um erro no upload. Tente novamente mais tarde.";
					customAlert.type="error";
					showAlert(customAlert);

					$(".dragover").removeClass("dragover");
		        },
		        done: function (e, returnData) {
		        	//When the upload are done
		        	var block=$(this).data("parentblock");
		        	var template_id = $(this).data("template");
					var turnAvaliable = $(this).data("turnavaliable");
					var markupData = Handlebars.compile($("#"+template_id).html());

		            $.each(returnData.result.files, function (index, file) {
		            	var template=markupData(file);
		                $(template).appendTo('#'+block+' .files');
		            });

		            if (returnData.result.preFiles) {
		            	var preFilesName = $(this).data("prefiles");

			            $.each(returnData.result.preFiles, function (index, dataId) {
			            	$("#main-form").append('<input type="hidden" name="'+preFilesName+'[]" id="preFiles-'+dataId+'" value="'+dataId+'">');
			            });
		            }

		            var maxFiles=$(this).data("maxfiles");
		            var uploadedFiles=$("#"+block).find(".files li");

		            if (maxFiles>0 && uploadedFiles.length>=maxFiles) {
		            	$("#"+block+" .fileinput-button").hide();
		            	$("#"+block+" .fileinput-alert").removeClass('hidden');
		            }

		            if (turnAvaliable.length) {
			            enableDisableElement(uploadedFiles,turnAvaliable);
		            }
					$(".dragover").removeClass("dragover");

		            setTimeout(function(){ $("#"+block+" .progress-bar").fadeOut(); }, 2000);
		        },
		        progressall: function (e, returnData) {
		        	var block=$(this).data("parentblock");

		        	//Update the progress bar of the upload
		            var progress = parseInt(returnData.loaded / returnData.total * 100, 10);
		            $("#"+block+" .progress-bar").css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	
	    //Delete button of the attachments block
		$(".upload-block").on("click", ".btn-delete-attach", function() {
			var button=$(this);
			var id=button.closest('li').data("id");
			var block=button.closest('.upload-block');
			var type=block.data("type");
			var maxFiles=block.find(".fileupload").data("maxfiles");
			var turnAvaliable = block.find(".fileupload").data("turnavaliable");
	        var url=block.find(".fileupload").data("url");
	        var customAlert={};

			//Execute Ajax
			$.ajax ({
			   type: "POST",
			   url: url+"/delete/",
			   dataType: 'json',
			   data:{ id: id },
			   success: function (data) {
			    if (data.status=="success") {
					$("#"+type+"-"+id).remove();
					if ($("#preFiles-"+id).length) {
						$("#preFiles-"+id).remove();
					}

					var uploadedFiles=block.find(".files li");
					if (maxFiles>0 && uploadedFiles.length<maxFiles) {
						block.find(".fileinput-button").show();
			        	block.find(".fileinput-alert").addClass('hidden');
					}

		            if (turnAvaliable.length) {
			            enableDisableElement(uploadedFiles,turnAvaliable);
		            }
			    } else {
			      button.prop("disabled",false);
			      customAlert.title="OOPS!";
			      customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
			      customAlert.type="error";
			      swal(customAlert.title, customAlert.message, customAlert.type);
			    }
			   },
			   error: function (data) {
			    button.prop("disabled",false);
			    customAlert.title="OOPS!";
			    customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
			    customAlert.type="error";
			    swal(customAlert.title, customAlert.message, customAlert.type);
			   }
			});
		});

		//Show de save button if the input is blured
		$(".upload-block").on("click", ".attach-desc", function(event){
			var inputItem=$(this);
			var parentBlock=inputItem.closest('.input-group');
			var saveBtn=parentBlock.find(".btn-save-attach");
			if (saveBtn.not(":visible")) {
				inputItem.data("original-text",inputItem.val());
				parentBlock.find(".btn-delete-attach").addClass('hidden');
				parentBlock.find(".btn-save-attach").removeClass('hidden');
				parentBlock.find(".btn-cancel").removeClass('hidden');
			}
		});

		//Cancel the edit of the input text of the attachment
		$(".upload-block").on("click", ".btn-cancel", function(event){
			var button=$(this);
			var parentBlock=button.closest('.input-group');
			var inputItem=parentBlock.find(".attach-desc");
			if (button.is(":visible")) {
				inputItem.val($(inputItem).data('original-text'));
				parentBlock.find(".btn-delete-attach").removeClass('hidden');
				parentBlock.find(".btn-save-attach").addClass('hidden');
				parentBlock.find(".btn-cancel").addClass('hidden');
			}
		});

		//Save the attachment updates
		$(".upload-block").on("click", ".btn-save-attach", function(event){
			var customAlert={};
			var button=$(this);
			button.prop("disabled",true);

			var parentBlock=button.closest('li');
			var id=parentBlock.data("id");
			var url=parentBlock.data("url");
			var subtitle=parentBlock.find(".attach-desc");

			$.ajax ({
				type: "POST",
				url: url+"/subtitle/",
				dataType: 'json',
				data:{name:subtitle.val(), id:id},
				success: function (data) {
					if (data.status==true) {
						parentBlock.find(".btn-delete-attach").removeClass('hidden');
						parentBlock.find(".btn-save-attach").addClass('hidden');
						parentBlock.find(".btn-cancel").addClass('hidden');
						$(subtitle).effect("highlight", {color: '#8BC34A'}, 2000);
					} else {
						customAlert.title="OOPS!";
						customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
						customAlert.type="error";
						swal(customAlert.title, customAlert.message, customAlert.type);
					}
				},
				error: function (data) {
					customAlert.title="OOPS!";
					customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
					customAlert.type="error";
					swal(customAlert.title, customAlert.message, customAlert.type);
				},
				complete: function (data) {
					button.prop("disabled",false);
				}
			});
		});

		//Make the attachment a highlight
		$(".upload-block").on("click", ".btn-highlight-attach", function(event){
			var customAlert={};
			var button=$(this);
			button.prop("disabled",true);

			var parentBlock=button.closest('li');
			var id=parentBlock.data("id");
			var url=parentBlock.data("url");
			var otherHighlight=parentBlock.closest('ul').find('.fa-star');

			$.ajax ({
				type: "POST",
				url: url+"/highlight/",
				data: {id:id},
				dataType: 'json',
				success: function (data) {
					if (data.status==true) {
						otherHighlight.removeClass('fa-star').addClass('fa-star-o');
						button.removeClass('fa-star-o').addClass('fa-star');
					} else {
						customAlert.title="OOPS!";
						customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
						customAlert.type="error";
						swal(customAlert.title, customAlert.message, customAlert.type);
					}
				},
				error: function (data) {
					customAlert.title="OOPS!";
					customAlert.message="Ocorreu um erro ao executar esta ação. Tente novamente mais tarde.";
					customAlert.type="error";
					swal(customAlert.title, customAlert.message, customAlert.type);
				},
				complete: function (data) {
					button.prop("disabled",false);
				}
			});
		});
	} 

	//Main functions to be executed
	fileUpload();
});