<?php
/**
 * Class ST_Contact
 *
* @package Storetrooper
**/
class ST_Contact
{
    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function add($params = array(), $additional = array(), $safe = true, $debug = true)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        $params['additional_info'] = $additional;

        return $curl->post('contact', $params, $safe, $debug);
    }

}