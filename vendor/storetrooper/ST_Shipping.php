<?php
/**
 * Class ST_Shipping
 *
* @package Storetrooper
**/
class ST_Shipping
{

    public function get($params = array())
    {
        $cart = Storetrooper::cart()->get();
        $params['products'] = array();

        if (isset($params['simulate'])){
            $params['products'] = array((object) array(
                'product' => $params['simulate'],
                'quantity' => isset($params['quantity']) ? (int) $params['quantity'] : 1
            ));
        } else {
            foreach ($cart as $key => $product){
                $params['products'][$key] = (object) array(
                    'product' => $product->id,
                    'quantity' => (int) $product->quantity
                );
            }
        }

        unset($params['simulate']);
        return ST_Curl::get('shipping/quote', $params, FALSE);
    }

}