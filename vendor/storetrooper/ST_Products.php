<?php
/**
 * Class ST_Products
 *
* @package Storetrooper
**/
class ST_Products
{

    /**
     * Get a json object with the store products
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        require_once "ST_Curl.php";
        $curl = new ST_Curl;

        $options = array(
            'limit'      => 12,
            'offset'     => 0,
            'filter'     => array(),
            'search'     => FALSE,
            'order'      => FALSE,
            'getFilters' => FALSE
        );
        $params = array_merge($options, $params);
        return $curl->get('products', $params, FALSE, (isset($params['getFilters']) && $params['getFilters']) ? FALSE : TRUE);
    }

    /**
     * Get a json object with the store products
     * @param  array      $params
     * @return json
     */
    public function get_variation($params = array())
    {
        require_once "ST_Curl.php";
        $curl = new ST_Curl;

        $options = array(
            'id_product'    => FALSE,
            'id_variation'  => FALSE,
        );
        $params = array_merge($options, $params);
        return $curl->get('products/get_variation', $params, FALSE, FALSE);
    }

    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function save_review($params = array())
    {
        require_once "ST_Curl.php";
        $curl = new ST_Curl;

        return $curl->post('products/save_review', $params);
    }
}