<?php
class user_Model {	
	function getUserData($user_email) {
		// Executa o comando no banco de dados
		global $db;
		$mysql = $db->prepare('SELECT * FROM users WHERE email="'.$user_email.'" LIMIT 1;');
		$mysql->execute();
		$retorno = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (!empty($retorno)) {
			return $retorno[0];
		} else {
			return false;
		}
	}

	function listUsers($role=false) {
		if ($role!==false) {
			$roleWhere='role>="'.$role.'"';
		} else {
			$roleWhere='role<>"0"';
		}
		// Executa o comando no banco de dados
		global $db;
		$sqlUsers='SELECT * FROM users WHERE '.$roleWhere.';';
		$mysql = $db->prepare($sqlUsers);
		$mysql->execute();
		$retorno = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (!empty($retorno)) {
			return $retorno;
		} else {
			return false;
		}
	}

	function getUser($userId) {
		// Executa o comando no banco de dados
		global $db;
		$mysql = $db->prepare('SELECT * FROM users WHERE id="'.$userId.'" LIMIT 1;');
		$mysql->execute();
		$retorno = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (!empty($retorno)) {
			return $retorno[0];
		} else {
			return false;
		}
	}
}
?>