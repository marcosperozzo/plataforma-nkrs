<?php
class site_Model {

	function getBanner($id) {
		global $db;

		$mysql = $db->prepare('
			SELECT
				banners.title,
				banners.content,
				banners.description,
				banners.youtube,
				attachments.file
			FROM banners
			INNER JOIN attachments ON banners.id = attachments.relation
			WHERE banners.id = ' . $id . '
			AND attachments.module = ' . 4
		);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}

	function getAllResults() {
		global $db;

		$mysql = $db->prepare('
			SELECT
				webcup.id,
				webcup.category,
				webcup.link,
				webcup.date
			FROM webcup
			WHERE YEAR(webcup.date) = '.date("Y").''
		);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}

	function getResultsById($id) {
		global $db;

		$mysql = $db->prepare('
			SELECT
				webcup.id,
				webcup.category,
				webcup.link,
				webcup.date
			FROM webcup
			WHERE webcup.id = '.$id.''
		);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}

	function getPage($id) {
		global $db;

		$mysql = $db->prepare('SELECT * FROM page WHERE id = ' . $id);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}

	function getAllCrowdfounding() {
		global $db;

		$mysql = $db->prepare('
			SELECT
				crowdfounding.id,
				crowdfounding.title,
				crowdfounding.desc,
				crowdfounding.value,
				crowdfounding.match,
				crowdfounding.link,
				crowdfounding.cover
			FROM crowdfounding'
		);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}

	function getCrowdfoundingById($id) {
		global $db;

		$mysql = $db->prepare('
			SELECT
				crowdfounding.id,
				crowdfounding.title,
				crowdfounding.desc,
				crowdfounding.value,
				crowdfounding.match,
				crowdfounding.link,
				crowdfounding.cover
			FROM crowdfounding
			WHERE crowdfounding.id = '.$id.''
		);
		$mysql->execute();
		return $mysql->fetchAll(PDO::FETCH_OBJ);
	}
}
?>
