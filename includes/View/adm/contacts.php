<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content tarefas">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white m-b">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" id="main-form" role="form" data-module="<?php echo $moduleName;?>" data-url="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">Nome</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="name" readonly value="<?php echo $itemContent->name;?>" placeholder="Nome" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="email">E-mail</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="email" readonly value="<?php echo $itemContent->email;?>" placeholder="E-mail" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="phone">Telefone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="phone" readonly value="<?php echo $itemContent->phone;?>" placeholder="Telefone" >
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-2 control-label" for="subject">Assunto</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="subject" readonly value="<?php echo $itemContent->subject;?>" placeholder="Assunto" >
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-2 control-label" for="business">Segmento do Negócio</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="business" readonly value="<?php echo $itemContent->business;?>" placeholder="Segmento do Negócio" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="date">Data de envio</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="date" readonly value="<?php echo convertDate($itemContent->date,"%d/%m/%Y %H:%M");?>" placeholder="Data de envio" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="message">Mensagem</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="message" rows="5" readonly><?php echo $itemContent->message;?></textarea>
                    </div>
                  </div>
                  <div class="form-group text-right">
                    <a href="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/" id="backBtn" class="btn btn-default btn-sm btn-icon loading-demo mr5" type="button">
                      <i class="icon-action-undo mr5"></i>
                      <span>Voltar</span>
                    </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
    <!-- /content panel -->
<?php include("_footer.php");?>
