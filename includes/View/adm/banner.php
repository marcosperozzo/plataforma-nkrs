<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content tarefas">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white m-b">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" id="main-form" role="form" data-module="<?php echo $moduleName;?>" data-url="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
				  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="title">Nome</label>
                    <div class="col-sm-10">
					  <input type="text" class="form-control" id="name"<?php if (empty($itemContent->id)) { ?> name="name" data-rule-required="true"<?php } else { ?> readonly<?php } ?> maxlength="255" value="<?php echo $itemContent->name;?>" placeholder="Nome do Banner" />
					</div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="title">Título</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="title" name="title" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->title;?>" placeholder="Título do banner" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="content">Conteúdo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="content" name="content" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->content;?>" placeholder="Conteúdo do banner" >
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="content">Descrição</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="description" name="description" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->description;?>" placeholder="Descrição do banner">
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="youtube">Link Youtube</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="youtube" name="youtube" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->youtube;?>" placeholder="Link do Video Youtube" >
                    </div>
                  </div>
                  <?php include("_upload.php"); ?>
                  <div class="form-group text-right">
                    <a href="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/" id="backBtn" class="btn btn-default btn-sm btn-icon loading-demo mr5" type="button">
                      <i class="icon-action-undo mr5"></i>
                      <span>Voltar</span>
                    </a>
                    <button class="btn btn-success btn-icon loading-demo mr5" disabled id="saveBtn" type="button">
                      <i class="icon-cursor mr5"></i>
                      <span>Salvar</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
    <!-- /content panel -->
<?php include("_footer.php");?>
