<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white">
          <div class="card-header">
            <div class="pull-left"><?php echo $tableHeading;?></div>
            <div class="text-right">
              <?php if (!empty($extraButton->url)) {?>
                <a class="btn btn-info btn-sm btn-icon mr5" href="<?php echo $extraButton->url;?>">
                  <i class="<?php echo $extraButton->icon;?>"></i>
                  <span><?php echo $extraButton->title;?></span>
                </a>
              <?php } ?>
              <?php if (!empty($deleteUrl)) { ?>
              <button class="btn btn-danger btn-sm btn-icon mr5" type="button" id="deleteBtn" data-url="<?php echo $deleteUrl;?>">
                <i class="icon-trash"></i>
                <span>Excluir selecionados</span>
              </button>
              <?php } ?>
              <?php if (!empty($addUrl)) {?>
                <a class="btn btn-success btn-sm btn-icon mr5" href="<?php echo $addUrl;?>">
                  <i class="icon-plus"></i>
                  <span>Adicionar</span>
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="card-block">
            <table class="table table-condensed table-striped responsive datatable m-b-0 <?php echo $additionalTableClass;?>">
              <thead>
                <tr>
                  <th style="width:40px;" class="nosort removeColumn" data-url="<?php echo $deleteUrl;?>"></th>
                  <?php foreach ($columns as $column) {
                    ?>
                    <th><?php echo $column;?></th>
                    <?php
                  }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($items as $item) { ?>
                  <tr data-id="<?php echo $item["id"];?>">
                    <?php if ($item["canDelete"]==true) { ?>
                      <td><input type="checkbox" name="delete[]" value="<?php echo $item["id"];?>"></td>
                    <?php } elseif ($item["canDelete"]==false) { ?>
                      <td><input type="checkbox" disabled=""></td>
                    <?php } ?>
                    <?php foreach ($item["columns"] as $column) {?>
                      <td>
                        <?php if ($item["canEdit"]) { ?>
                          <a href="<?php echo $editUrl;?><?php echo $item["id"];?>"><?php echo $column;?></a>
                        <?php } else { ?>
                          <a href="#"><?php echo $column;?></a>
                        <?php } ?>
                      </td>
                    <?php } ?>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /main area -->
<?php include("_footer.php");?>