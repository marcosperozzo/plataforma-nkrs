<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content tarefas">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white m-b">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" id="main-form" role="form" data-module="<?php echo $moduleName;?>" data-url="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="status">Status</label>
                    <div class="col-sm-10">
                      <select data-placeholder="Escolha a opção" class="chosen" name="status" id="status" style="width: 100%;" data-rule-required="true">
                        <option value=""></option>
                        <option value="0"<?php if ($itemContent->status=="0") { ?> selected<?php } ?>>Inativo</option>
                        <option value="1"<?php if ($itemContent->status=="1") { ?> selected<?php } ?>>Ativo</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">Nome</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="name" name="name" maxlength="255" value="<?php echo $itemContent->name;?>" placeholder="Nome relacionado ao cadastro" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="email">E-mail</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="email" name="email" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->email;?>" placeholder="E-mail do usuário" >
                    </div>
                  </div>
                  <?php if (!empty($itemContent->created)) { ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="created">Adicionado em</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="created" readonly="" value="<?php echo convertDate($itemContent->created,"%d/%m/%Y - %H:%M");?>" placeholder="Data de adição do e-mail no banco de dados" >
                    </div>
                  </div>
                  <?php } ?>
                  <div class="form-group text-right">
                    <a href="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/" id="backBtn" class="btn btn-default btn-sm btn-icon loading-demo mr5" type="button">
                      <i class="icon-action-undo mr5"></i>
                      <span>Voltar</span>
                    </a>
                    <button class="btn btn-success btn-icon loading-demo mr5" id="saveBtn" type="button">
                      <i class="icon-cursor mr5"></i>
                      <span>Salvar</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
    <!-- /content panel -->
<?php include("_footer.php");?>