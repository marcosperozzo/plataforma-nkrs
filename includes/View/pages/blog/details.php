<section id="blog-details">
    <div class="container-fluid blog__header" style="background-image: url(<?= $post->gallery[0]->path ?>)">
        <div class="blog__header__content">
            <h1><?=$post->title?></h1>
            <p><?=$post->date?></p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <!-- BLOG CONTENT -->
                <div class="blog__category">
                    <span><?=$post->category?></span>
                </div>
                <div class="blog__content">
                    <?=$post->description?>
                </div>
                <?php if(count($post->gallery) > 1) { ?>
                    <div id="sliderAbout1" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <?php for($i = 0; $i < count($post->gallery); $i++) { ?>
                                <li data-target="#sliderAbout1" data-slide-to="<?=$i?>" class="<?=$i == 0 ? 'active' : ''?>"></li>
                            <?php } ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach($post->gallery as $key => $photo) { ?>
                                <div class="carousel-item <?=$key == 0 ? 'active' : ''?>" style="background-image: url(<?=$photo->path?>);">
                                </div>
                            <?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#sliderAbout1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#sliderAbout1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                <?php } ?>
                <div class="separator">
                    <div class="separator__bar"></div>
                </div>
                <section id="blog">
                    <h3 class="section-title">Veja também</h3>
                    <?php if(!empty($posts)){ ?>
                        <?php foreach($posts as $post) { ?>
                            <a href="<?=$endereco_site?>blog/detalhes?slug=<?=$post->slug?>" class="row box-news">
                                <div class="col-sm-4 col-md-3 box-news__thumb" style="background-image: url(<?= $post->image ?>)"></div>
                                <div class="col-sm-8 box-news__info">
                                    <span class="box-news__category"><?=$post->category?></span>
                                    <p class="box-news__title"><?=$post->title?></p>
                                    <p class="box-news__date"><?=$post->date?></p>
                                </div>
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        <h5>Não foi possível carregar mais posts</h5>
                    <?php } ?>
                </section>
            </div>
            <div class="col-lg-4 side-content">
                <?php include ENDERECO_FISICO."/includes/View/components/aside.php"; ?>
            </div>
        </div>
    </div>
</section>