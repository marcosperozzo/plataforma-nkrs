<div class="panel">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- MENU DE AÇÕES -->
                <?php include 'menu.php'; ?>
            </div>
            <div class="col-md-9">
                <!-- CONTEÚDO VINDO DO CONTROLLER -->
                <?php include $content; ?>
            </div>
        </div>
    </div>
</div>