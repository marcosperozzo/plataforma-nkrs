<section class="container">
    <h3 class="section-title section-title--h">Resultados - <?=date("Y")?></h3>
    <?php foreach($data as $item){ ?>
        <div class="row">
            <div class="col-12">
                <h3 class="section-title"><?=$item->category?></h3>
                <a href="<?=$endereco_site?>painel/webcup-editar?id=<?=$item->id?>" class="btn btn-primary webcup-edit">Editar</a>
                <iframe class="webcup-iframe" src="<?=$item->link?>" frameborder="0"></iframe>
            </div>
        </div>
    <?php } ?>
</section>