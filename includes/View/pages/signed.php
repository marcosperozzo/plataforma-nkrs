<section id="signed" class="signed">
    <div class="signed__item signed__item--left" style="background-image: url('<?=$endereco_site?>assets/site/images/inscricoesGP.jpg')">
        <div class="container">
            <div class="signed__item__info col-sm-5">
                <h3 class="section-title">Categoria GP</h3>
                <h4>R$220,00</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed neque risus. Vestibulum odio ipsum, consequat vitae nunc non, scelerisque auctor leo. Praesent in tellus condimentum, pulvinar nisl in, rutrum dui. Nullam euismod ex nec molestie feugiat. Pellentesque vel tellus ut ligula luctus bibendum non sed neque. Suspendisse quis tincidunt elit. Etiam pharetra sem nec mi ultrices tempor.</p>
                <button type="button" class="btn btn-lg btn-danger" data-cart data-url="<?=$endereco_site?>cart/add">Quero me inscrever</button>
            </div>
        </div>
    </div>
    <div class="signed__item signed__item--right" style="background-image: url('<?=$endereco_site?>assets/site/images/inscricoesPRO.jpg')">
        <div class="container">
            <div class="signed__item__info col-sm-5">
                <h3 class="section-title section-title--right">Categoria PRO</h3>
                <h4>R$270,00</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed neque risus. Vestibulum odio ipsum, consequat vitae nunc non, scelerisque auctor leo. Praesent in tellus condimentum, pulvinar nisl in, rutrum dui. Nullam euismod ex nec molestie feugiat. Pellentesque vel tellus ut ligula luctus bibendum non sed neque. Suspendisse quis tincidunt elit. Etiam pharetra sem nec mi ultrices tempor.</p>
                <button type="button" class="btn btn-lg btn-danger" data-cart data-url="<?=$endereco_site?>cart/add">Quero me inscrever</button>
            </div>
        </div>    
    </div>
    <div class="signed__item signed__item--left" style="background-image: url('<?=$endereco_site?>assets/site/images/inscricoesELITE.jpg')">
        <div class="container">
            <div class="signed__item__info col-sm-5">
                <h3 class="section-title">Categoria ELITE</h3>
                <h4>R$350,00</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed neque risus. Vestibulum odio ipsum, consequat vitae nunc non, scelerisque auctor leo. Praesent in tellus condimentum, pulvinar nisl in, rutrum dui. Nullam euismod ex nec molestie feugiat. Pellentesque vel tellus ut ligula luctus bibendum non sed neque. Suspendisse quis tincidunt elit. Etiam pharetra sem nec mi ultrices tempor.</p>
                <button type="button" class="btn btn-lg btn-danger" data-cart data-url="<?=$endereco_site?>cart/add">Quero me inscrever</button>
            </div>
        </div>
    </div>
</section>