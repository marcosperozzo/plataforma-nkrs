<section class="page container">
    <h3 class="section-title">Contato</h3>
    <p>Você ficou com dúvidas ou quer mandar uma mensagem pra nós, aproveite o espaço abaixo.</p>
    <form class="form" method="POST" action="<?=$endereco_site?>contact/save" data-validate>
        <input type="text" class="form-control" name="name" placeholder="Nome *" required />
        <input type="email" class="form-control" name="email" placeholder="E-mail *" required />
        <input type="tel" class="form-control" name="phone" placeholder="Telefone *" required />
        <textarea class="form-control" name="additional_info" rows="7" placeholder="Mensagem *" required></textarea>
        <div class="form__actions">
            <button type="button" class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
</section>