<?php
class signed_Controller extends common_Controller {

	function index(){
		$data["title"] = "Inscrições - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/signed","layout/_footer"),$data);
	}
}
?>
