<?php
class address_Controller extends common_Controller {

	function save()
	{
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Addresses.php";
		$storetrooper = new ST_Addresses;

		postRequest();

		if(!empty($_POST["id"])){
			$data["id"] = $_POST["id"];
		}
		$data["client"] = intval($_POST["id_client"]);
		$data["label"] = $_POST["label"];
		$data["zipcode"] = $_POST["zipcode"];
		$data["street"] = $_POST["street"];
		$data["number"] = $_POST["number"];
		$data["complement"] = isset($_POST["complement"]) ? $_POST["complement"] : null;
		$data["suburb"] = $_POST["suburb"];
		$data["city"] = intval($_POST["city"]);
		$data["additional_info"] = $_POST["additional_info"];
		$data["default"] = $_POST["default"];
		$data["billing"] = $_POST["billing"];

		if(empty($data["id"])){
			// Enviar dados para a API
			$response = json_decode($storetrooper->add($data));
			if($response->status == true){
				$response->message = "Endereço cadastrado com sucesso!";
				$response->url = ENDERECO_SITE."painel/enderecos";
			}
		} else {
			// Enviar dados para a API
			$response = json_decode($storetrooper->edit($data));
			if($response->status == true){
				$response->message = "Endereço editado com sucesso!";
				$response->url = ENDERECO_SITE."painel/enderecos";
			}
		}

		echo json_encode($response);
	}

	function delete()
	{
		session_start();
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Addresses.php";
		$storetrooper = new ST_Addresses;

		postRequest();

		$data["id"] = intval($_POST["id"]);
		$data["client"] = intval($_SESSION['nkrs']['client']['id']);

		$response = json_decode($storetrooper->delete(json_encode($data)));

		echo json_encode($response);
	}

	function get_cities()
	{
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Utils.php";
		$storetrooper = new ST_Utils;

		postRequest();

		$state = $_POST['state'];
		$response = json_decode($storetrooper->get_cities($state));
		
		echo json_encode($response);
	}
}
?>
