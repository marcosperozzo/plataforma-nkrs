<?php
class cart_Controller extends common_Controller {

	function index() {
		//require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Blog.php";
		//$storetrooper = new ST_Blog;

		// busca a pagina para preencher SEO
		$data["title"] = "Carrinho de compras - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";
		//Passando o limite de posts como array
		// $response = json_decode($storetrooper->get(array('limit' => 10)));

		// foreach($response->data as $key => $post){
		// 	$response->data[$key]->image = str_replace('/st/','/adsites/',$post->image);
		// }
			
		// if($response->status == true){
		// 	$data['posts'] = $response->data;
		// } else {
		// 	$data['nodata'] = "Não foi possível carregar os posts";
		// }
		
		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/cart","layout/_footer"),$data);
	}

	function add(){

		if(isset($_POST)){
			require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Cart.php";
			$st_cart = new ST_Cart;

			$response = json_decode($st_cart->add(array('product' => intval($_POST['product']), 'quantity' => intval($_POST['quantity']), 'additional' => intval($_POST['additional']), 'optional' => intval($_POST['optional']))));
			
			die(var_dump($_SESSION['nkrs']['cart']));
		} else {
			$response['status'] = false;
			$response['message'] = "Não foi possível adicionar o produto ao carrinho";
			echo json_encode($response);
			exit;
		}
	}
}
?>
