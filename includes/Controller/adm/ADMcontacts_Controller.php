<?php
class ADMcontacts_Controller extends ADMcommon_Controller {

	protected $moduleName="contacts";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}
	
	function index($alertData=false) {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Contatos do site";
		$data["pageSubHeading"]="Visualize os contatos enviados pelo site";
		$data["tableHeading"]="Lista de contatos";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->listaItens($this->moduleName,0,999999999,false,"no");

		//Adjust the itens to show on the table body
		$data["items"]=array();
		foreach ($dataReturn as $item) {
			$dataArray["id"]=$item->id;
			$dataArray["canEdit"]=true;
			$dataArray["canDelete"]=true;
			$dataArray["columns"][0]=$item->name;
			$dataArray["columns"][1]=convertDate($item->date,"%d/%m/%Y %H:%M");
			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="Nome";
		$data["columns"][1]="Data";

		//Set the actions URLs
		$data["editUrl"]=$endereco_site."adm/".$this->moduleName."/edit/";
		$data["deleteUrl"]=$endereco_site."adm/".$this->moduleName."/delete/";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}

	function edit($urlData) {
		$itemId=$urlData[0];
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Visualização de contato";
		$data["pageSubHeading"]="Visualize um contato enviado pelo site";
		$data["contentHeading"]="Dados do contato";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$data["itemContent"]=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (!$data["itemContent"]) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou foi excluído.";
			$this->index($alertData);
			die;
		}

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/contacts",$data);
	}

	function delete($urlData) {
		postRequest();
		$userData=$this->getUserData();
		$endereco_fisico=$this->endereco_fisico;

		$itemId=$urlData[0];

		$modelGeral=new geral_Model();
		$itemContent=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (empty($itemContent)) {
			$jsonReturn["status"]="error";
			$jsonReturn["message"]="Parece que este registro não existe";
			die;
		} else {
			dbDelete($this->moduleName,$itemId);
			$jsonReturn["status"]="ok";
			$jsonReturn["id"]=$itemId;

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="delete";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$itemId;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			echo newJSON($jsonReturn);
			die;
		}
	}
}
?>