<?php
class ADMusers_Controller extends ADMcommon_Controller {

	protected $moduleName="users";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName,true);
		parent::__construct();
	}

	function index($alertData=false) {
		$this->verifyRole(array(0,1));

		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Usuários";
		$data["pageSubHeading"]="Controle o cadastro dos usuários da equipe de sua empresa";
		$data["tableHeading"]="Lista de usuários";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results from API
		$modelUser=new user_Model();
		$dataReturn=$modelUser->listUsers($data["userData"]["role"]);

		//Adjust the itens to show on the table body
		$data["items"]=array();
		foreach ($dataReturn as $item) {
			$dataArray["id"]=$item->id;
			$dataArray["canEdit"]=true;
			if ($data["userData"]["id"]==$item->id) {
				$dataArray["canDelete"]=false;
			} else {
				$dataArray["canDelete"]=true;
			}
			$dataArray["columns"][0]=$item->name;
			$dataArray["columns"][1]=$item->email;
			if ($item->role=="0") {
				$dataArray["columns"][2]="Weecom";
			} elseif ($item->role=="1") {
				$dataArray["columns"][2]="Administrador";
			} elseif ($item->role=="2") {
				$dataArray["columns"][2]="Editor";
			}
			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="Nome";
		$data["columns"][1]="E-mail";
		$data["columns"][2]="Perfil";

		//Set the actions URLs
		$data["addUrl"]=$endereco_site."adm/".$this->moduleName."/add/";
		$data["editUrl"]=$endereco_site."adm/".$this->moduleName."/edit/";
		$data["deleteUrl"]=$endereco_site."adm/".$this->moduleName."/delete/";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}

	function add() {
		$this->verifyRole(array(0,1));

		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Adicionar usuário";
		$data["pageSubHeading"]="Adicione um usuário à equipe sua empresa.";
		$data["contentHeading"]="Formulário de cadasto";
		
		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		$data["token"]=$data["userData"]["token"];

		$data["attachments"][0]["label"]="Enviar avatar";
		$data["attachments"][0]["type"]="image";
		$data["attachments"][0]["block"]="banners";
		$data["attachments"][0]["iconSize"]="fa-4x";
		$data["attachments"][0]["url"]=$endereco_site."adm/attachments";
		$data["attachments"][0]["maxFiles"]=1;
		$data["attachments"][0]["fileTypesText"]="Permitido envio de JPG e PNG";
		$data["attachments"][0]["fileTypes"]="image/jpg,image/jpeg,image/png";
		$data["attachments"][0]["templateName"]="template-uploaded";
		$data["attachments"][0]["turnAvaliable"]=false;
		$data["attachments"][0]["preFiles"]="preimage";
		$data["attachments"][0]["module"]=$this->getModuleId($this->moduleName);
		$data["attachments"][0]["relation"]=false;
		$data["attachments"][0]["list"]=false;
		$data["templates"]=array("_tpl_attachment_list.php");

		//Load the view
		loadView("adm/user",$data);
	}
	function edit($urlData,$myInfo=false) {
		if (!$myInfo) {
			$this->verifyRole(array(0,1));
		}

		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;
		$itemId=(int)$urlData[0];

		//Page Data
		if ($myInfo) {
			$data["pageHeading"]="Editar meus dados";
			$data["pageSubHeading"]="Edite os dados do seu usuário.";
		} else {
			$data["pageHeading"]="Editar usuário";
			$data["pageSubHeading"]="Edite um usuário previamente cadastrado em sua empresa.";
		}
		$data["contentHeading"]="Formulário de edição";
		
		$data["myInfo"]=$myInfo;

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch client data
		$modelUser=new user_Model();
		$dataReturn=$modelUser->getUser($itemId);
		if (empty($dataReturn)) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou já foi excluído";
			$this->index($alertData);
			die;
		}
		$data["itemContent"]=$dataReturn;

		$data["attachments"][0]["label"]="Enviar avatar";
		$data["attachments"][0]["type"]="image";
		$data["attachments"][0]["block"]="banners";
		$data["attachments"][0]["iconSize"]="fa-4x";
		$data["attachments"][0]["url"]=$endereco_site."adm/attachments";
		$data["attachments"][0]["maxFiles"]=1;
		$data["attachments"][0]["fileTypesText"]="Permitido envio de JPG e PNG";
		$data["attachments"][0]["fileTypes"]="image/jpg,image/jpeg,image/png";
		$data["attachments"][0]["templateName"]="template-uploaded";
		$data["attachments"][0]["turnAvaliable"]=false;
		$data["attachments"][0]["preFiles"]="preimage";
		$data["attachments"][0]["module"]=$this->getModuleId($this->moduleName);
		$data["attachments"][0]["relation"]=$data["itemContent"]->id;
		$data["attachments"][0]["list"]=loadMedia($data["attachments"][0]["type"],$data["attachments"][0]["module"],$data["attachments"][0]["relation"],$data["attachments"][0]["block"]);
		$data["templates"]=array("_tpl_attachment_list.php");

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/user",$data);
	}
	function myInfo() {
		$userDataFull=$this->getUserData();
		$userData[0]=$userDataFull["id"];

		$this->edit($userData,true);
	}
	function save() {
		postRequest();
		$userData["name"]=$_POST["name"];
		$userData["email"]=$_POST["email"];
		if ($_POST["role"]!==false) {
			$userData["role"]=$_POST["role"];
		}
		if (!empty($_POST["password"])) {
			$userData["password"]=password_hash($_POST["password"], PASSWORD_DEFAULT);
		}

		unset($_POST["files"]);
		unset($_POST["mentions"]);
		unset($_POST["module"]);
		$preFiles=$_POST["preimage"];
		unset($_POST["preimage"]);
		unset($_POST["relation"]);
		unset($_POST["type"]);
		unset($_POST["block"]);

		if (empty($_POST["id"])) {
			if (dbDuplicated("users",array("email"=>$userData["email"]),"bool") and !empty($userData["email"])) {
				$jsonReturn["status"]="error";
				$jsonReturn["message"]="Este e-mail já está em uso. Tente um outro endereço.";
			} else {
				$userId=dbSave("users",$userData,true);

				if (!empty($preFiles)) {
					foreach ($preFiles as $fileId) {
						$fileData["relation"]=$userId;
						$fileData["id"]=$fileId;
						dbUpdate("attachments",$fileData);
					}
				}

				$jsonReturn["status"]="ok";
				$jsonReturn["created_id"]=$userId;
				$jsonReturn["message"]="Usuário adicionado com sucesso";
			}
		} else {
			if (dbDuplicated("users",array("email"=>$userData["email"],"id"=>$_POST["id"]),"bool") and !empty($userData["email"])) {
				$jsonReturn["status"]="error";
				$jsonReturn["message"]="Este e-mail já está em uso. Tente um outro endereço.";
			} else {
				$userData["id"]=$_POST["id"];
				dbUpdate("users",$userData);
				$jsonReturn["status"]="ok";
				$jsonReturn["message"]="Usuário editado com sucesso";
			}
		}

		echo newJSON($jsonReturn);
		die;
	}
	function delete($dadosURL) {
		$this->verifyRole(array(0,1));
		$userData=$this->getUserData();
		$endereco_fisico=$this->endereco_fisico;

		$idItem=(int)$dadosURL[0];

		$attachments=loadMedia("attachment",$this->getModuleId($this->moduleName),$itemId);
		foreach ($attachments as $attachmentData) {
			$filePath=$endereco_fisico."/uploads/".$attachmentData->type."/".$attachmentData->file;
			if (file_exists($filePath)) {
				unlink($filePath);
			}
			dbDelete("attachments",$attachmentData->id);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="delete";
			$logData["tablename"]="attachments";
			$logData["item"]=$attachmentData->id;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);
		}

		dbDelete("users",$idItem);
		$jsonReturn["status"]="ok";
		$jsonReturn["id"]=$idItem;

		echo newJSON($jsonReturn);
		die;
	}
}
?>