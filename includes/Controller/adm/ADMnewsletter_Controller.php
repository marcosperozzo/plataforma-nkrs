<?php
class ADMnewsletter_Controller extends ADMcommon_Controller {

	protected $moduleName="newsletter";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}
	
	function index($alertData=false) {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Base de e-mails";
		$data["pageSubHeading"]="Controle os e-mails das pessoas que se cadastraram para receber novidades no blog";
		$data["tableHeading"]="Lista de e-mails";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->listaItens($this->moduleName,0,999999999,false);

		//Adjust the itens to show on the table body
		$data["items"]=array();
		foreach ($dataReturn as $item) {
			$dataArray["id"]=$item->id;
			$dataArray["canEdit"]=true;
			$dataArray["canDelete"]=true;
			$dataArray["columns"][0]=$item->email;
			if ($item->status==1) {
				$dataArray["columns"][1]="Sim";
			} else {
				$dataArray["columns"][1]="Não";
			}
			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="E-mail";
		$data["columns"][1]="Ativo?";

		//Set the actions URLs
		$data["addUrl"]=$endereco_site."adm/".$this->moduleName."/add/";
		$data["editUrl"]=$endereco_site."adm/".$this->moduleName."/edit/";
		$data["deleteUrl"]=$endereco_site."adm/".$this->moduleName."/delete/";

		$data["extraButton"]->url=$endereco_site."adm/".$this->moduleName."/export/";
		$data["extraButton"]->icon="fa fa-share-square-o";
		$data["extraButton"]->title="Exportar lista";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}

	function add() {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Adição de e-mail";
		$data["pageSubHeading"]="Inclua um novo e-mail na base de dados. Lembre-se que seus clientes devem ter autorizado o recebimento de e-mails.";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/newsletter",$data);
	}

	function edit($urlData) {
		$itemId=$urlData[0];
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Edição de e-mail";
		$data["pageSubHeading"]="Edite um e-mail previamente cadastrado no banco de dados. Lembre-se que apenas os usuários ativos serão exportados na lista.";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$data["itemContent"]=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (!$data["itemContent"]) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou foi excluído.";
			$this->index($alertData);
			die;
		}

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/newsletter",$data);
	}

	function save() {
		postRequest();
		$userData=$this->getUserData();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;
		
		$modelGeral=new geral_Model();

		if (empty($_POST["id"])) {
			$itemData["name"]=$_POST["name"];
			$itemData["email"]=$_POST["email"];
			$itemData["status"]=$_POST["status"];

			$itemId=dbSave($this->moduleName,$itemData,true);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="save";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$itemId;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			$jsonReturn["status"]="ok";
			$jsonReturn["created_id"]=$itemId;
			$jsonReturn["message"]="E-mail adicionado com sucesso";
		} else {
			$itemData["id"]=$_POST["id"];
			$itemData["name"]=$_POST["name"];
			$itemData["email"]=$_POST["email"];
			$itemData["status"]=$_POST["status"];
			dbUpdate($this->moduleName,$itemData);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="update";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$_POST["id"];
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			$jsonReturn["status"]="ok";
			$jsonReturn["message"]="E-mail editado com sucesso";
		}

		echo newJSON($jsonReturn);
		die;
	}

	function delete($urlData) {
		postRequest();
		$userData=$this->getUserData();
		$endereco_fisico=$this->endereco_fisico;
		$endereco_site=$this->endereco_site;

		$itemId=$urlData[0];

		dbDelete($this->moduleName,$itemId);
		$jsonReturn["status"]="ok";
		$jsonReturn["id"]=$itemId;

		//Log Action
		$logData["user_id"]=$userData["id"];
		$logData["action"]="delete";
		$logData["tablename"]=$this->moduleName;
		$logData["item"]=$itemId;
		$logData["date"]=date("Y-m-d H:i:s");
		dbSave("logs",$logData);

		echo newJSON($jsonReturn);
		die;
	}

	function export() {
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->listaItens($this->moduleName,0,999999999,false,"1");
		
		header("Content-type: text/csv, charset=UTF-8; encoding=UTF-8'");  
		header("Cache-Control: no-store, no-cache");  
		header('Content-Disposition: attachment; filename="newsletter.csv"');  

		//Print the header
		echo "Nome,Email,Adicionado em\n";
		
		//Print the e-mails
		foreach ($dataReturn as $newsletter) {
			echo "'".$newsletter->name."';'".$newsletter->email."';'".convertDate($newsletter->created,"%d/%m/%Y %H:%M")."'\n";
		}

		die();
	}
}
?>