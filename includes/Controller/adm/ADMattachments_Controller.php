<?php
class ADMattachments_Controller extends ADMcommon_Controller {

	protected $moduleName="attachments";

	function index() {
		postRequest();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;
		$userData=$this->getUserData();

		$jsonReturn["files"]=array();
		$jsonReturn["preFiles"]=array();

		$maxWidth="1920";
		$maxHeight="1080";
		$quality="85";

		$files=array();
		$fdata=$_FILES["files"];
		if(is_array($fdata['name'])){
			for($i=0;$i<count($fdata['name']);++$i){
				$files[]=array(
					'name'=>$fdata['name'][$i],
					'type'=> $fdata['type'][$i],
					'tmp_name'=>$fdata['tmp_name'][$i],
					'error'=>$fdata['error'][$i], 
					'size'=>$fdata['size'][$i]  
				);
		    }
		} else {
			$files[]=$fdata;
		}

		foreach ($files as $file) {		
			$tempFile = $file['tmp_name'];
			$targetPath = $endereco_fisico."/uploads/".$_POST["type"];
			
			$extension = getExtension($file['name']);
			$fileName = uniqid (rand ()) . "." . $extension;

			$targetFile =  $targetPath ."/". $fileName;

			move_uploaded_file($tempFile,$targetFile);
			
			if ($_POST["type"]=="image") {
				$img = false;
				
				if ($extension == 'jpg' || $extension == 'jpeg') {
					$img = imagecreatefromjpeg($targetFile);
				} else if ($extension == 'png') {
					$img = imagecreatefrompng($targetFile);
				} else if ($extension == 'gif') {
					$img = imagecreatefromgif($targetFile);
				}
				
				if ($img) {
					$width  = imagesx($img);
					$height = imagesy($img);
					$scale  = min($maxWidth/$width, $maxHeight/$height);
				
					if ($scale < 1) {
						$new_width = floor($scale*$width);
						$new_height = floor($scale*$height);
				
						$tmp_img = imagecreatetruecolor($new_width, $new_height);
				
						imagecopyresampled($tmp_img, $img, 0, 0, 0, 0,$new_width, $new_height, $width, $height);
						imagedestroy($img);
						$img = $tmp_img;
						
						imagejpeg($img, $targetFile, $quality);
					}
				}
			}

			$itemData["name"]=$file["name"];
			$itemData["file"]=$fileName;
			$itemData["module"]=$_POST["module"];
			if (!empty($_POST["relation"])) {
				$itemData["relation"]=$_POST["relation"];
			}
			$itemData["type"]=$_POST["type"];
			$itemData["block"]=$_POST["block"];
			$itemData["x"]=0;
			$itemData["y"]=0;
			$itemData["width"]=0;
			$itemData["height"]=0;
			$itemData["highlight"]=0;
			$itemData["item_order"]=0;
			$itemId=dbSave($this->moduleName,$itemData,true);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="save";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$itemId;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			if (empty($_POST["relation"]) or $_POST["relation"]=="0") {
				array_push($jsonReturn["preFiles"], $itemId);
			}

			$jsonData["id"]=$itemId;
			$jsonData["name"]=$file["name"];
			if ($_POST["type"]=="image" or $extension=="jpg" or $extension=="jpeg" or $extension=="png" or $extension=="gif" or $extension=="bmp") {
				$imagePath=showImage($itemId,300,280,false,"FFFFFF");
				$jsonData["element"]="<img src='".$imagePath."' class='img-responsive'>";
			} else {
				$jsonData["element"]="<i class='fa fa-file fa-4x'></i>";
			}
			$jsonData["path"]=$endereco_site."/uploads/".$_POST["type"]."/".$fileName;
			$jsonData["type"]=$_POST["type"];
			$jsonData["highlight"]=false;
			array_push($jsonReturn["files"], $jsonData);
		}
		
		echo newJSON($jsonReturn);
		die;
	}

	function delete() {
		postRequest();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;

		$modelGeral=new geral_Model();
		$attachmentData=$modelGeral->dataItem($this->moduleName, array("id"=>$_POST["id"]));
		$filePath=$endereco_fisico."/uploads/".$attachmentData->type."/".$attachmentData->file;
		if (file_exists($filePath)) {
			unlink($filePath);
		}
		dbDelete($this->moduleName,$_POST["id"]);

		//Log Action
		$userData=$this->getUserData();
		$logData["user_id"]=$userData["id"];
		$logData["action"]="delete";
		$logData["tablename"]=$this->moduleName;
		$logData["item"]=$_POST["id"];
		$logData["date"]=date("Y-m-d H:i:s");
		dbSave("logs",$logData);

		echo newJSON(array("status"=>"success"));
		die;
	}

	function order() {
		postRequest();

		$items=$_POST["items"];
		$order=1;
		foreach ($items as $item) {
			$dataAttachment["item_order"]=$order;
			$dataAttachment["id"]=$item;
			dbUpdate("attachments",$dataAttachment);

			$order++;
		}

		$jsonReturn["status"]=true;
		$jsonReturn["message"]="Ordem alterada com sucesso.";

		echo newJSON($jsonReturn);
		die;
	}

	function subtitle() {
		postRequest();

		if (!empty($_POST["id"]) and !empty($_POST["name"])) {
			$attachData["id"]=(int)$_POST["id"];
			$attachData["name"]=$_POST["name"];
			dbUpdate("attachments",$attachData);

			$jsonReturn["status"]=true;
			$jsonReturn["message"]="Legenda alterada com sucesso.";
		} else {
			$jsonReturn["status"]=false;
			$jsonReturn["message"]="Erro ao receber informações.";
		}

		echo newJSON($jsonReturn);
		die;
	}

	function highlight() {
		postRequest();

		if (!empty($_POST["id"])) {
			$modelGeral=new geral_Model();

			$attachData=$modelGeral->dataItem("attachments",array("id"=>(int)$_POST["id"]));
			$allAttachments=$modelGeral->listaSQL("SELECT * FROM attachments WHERE module='".$attachData->module."' AND relation='".$attachData->relation."' AND type='".$attachData->type."' AND block='".$attachData->block."';");

			foreach ($allAttachments as $attach) {
				$attachUnHighlight["id"]=$attach->id;
				$attachUnHighlight["highlight"]="0";
				dbUpdate("attachments",$attachUnHighlight);
			}

			$attachHighlight["id"]=(int)$_POST["id"];
			$attachHighlight["highlight"]="1";
			dbUpdate("attachments",$attachHighlight);

			$jsonReturn["status"]=true;
			$jsonReturn["message"]="Legenda alterada com sucesso.";
		} else {
			$jsonReturn["status"]=false;
			$jsonReturn["message"]="Erro ao receber informações.";
		}

		echo newJSON($jsonReturn);
		die;
	}
}
?>