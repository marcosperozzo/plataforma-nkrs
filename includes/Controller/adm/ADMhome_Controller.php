<?php
class ADMhome_Controller extends ADMcommon_Controller {

	protected $moduleName="home";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}

	function index() {
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]="Dashboard - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]="home";
		$data["moduleName"]=$this->moduleName;

		$modelGeral=new geral_Model();
		$data["collum1"]=$modelGeral->listaSQL("SELECT COUNT(*) AS total FROM contacts;",true);
		$data["collum1"]->title="Contatos";
		$data["collum1"]->icon="fa-paper-plane";

		$data["collum2"]=$modelGeral->listaSQL("SELECT COUNT(*) AS total FROM users WHERE role<>'0';",true);
		$data["collum2"]->title="Usuários";
		$data["collum2"]->icon="fa-users";

		$data["collum3"]=$modelGeral->listaSQL("SELECT COUNT(*) AS total FROM banners;",true);
		$data["collum3"]->title="Banners";
		$data["collum3"]->icon="fa-star";

		loadView("adm/index",$data);
	}
}
?>
