<?php
class ADMgeneral_Controller extends ADMcommon_Controller {

	protected $moduleName="general";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}
	
	function index() {
		$itemId="1";
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Dados gerais";
		$data["pageSubHeading"]="Edite os dados gerais da empresa";
		$data["contentHeading"]="Formulário de informações";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$data["itemContent"]=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (!$data["itemContent"]) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou foi excluído.";
			$this->index($alertData);
			die;
		}

		$data["states"]=$modelGeral->listaSQL("SELECT * FROM states WHERE status='1';");
		$data["cities"]=$modelGeral->listaSQL("SELECT * FROM cities WHERE status='1' AND state='".$data["itemContent"]->state."';");

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/general",$data);
	}

	function save() {
		postRequest();
		$userData=$this->getUserData();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;
		
		$modelGeral=new geral_Model();

		unset($_POST["files"]);
		unset($_POST["mentions"]);
		unset($_POST["module"]);
		$preFiles=$_POST["prefiles"];
		unset($_POST["prefiles"]);
		unset($_POST["relation"]);
		unset($_POST["type"]);
		unset($_POST["block"]);

		$itemData["id"]=$_POST["id"];
		$itemData["address"]=$_POST["address"];
		$itemData["neighborhood"]=$_POST["neighborhood"];
		$itemData["city"]=$_POST["city"];
		$itemData["state"]=$_POST["state"];
		$itemData["phone"]=$_POST["phone"];
		$itemData["mobile"]=$_POST["mobile"];
		$itemData["facebook"]=addScheme($_POST["facebook"]);
		$itemData["gmaps"]=$_POST["gmaps"];
		dbUpdate($this->moduleName,$itemData);

		//Log Action
		$logData["user_id"]=$userData["id"];
		$logData["action"]="update";
		$logData["tablename"]=$this->moduleName;
		$logData["item"]=$_POST["id"];
		$logData["date"]=date("Y-m-d H:i:s");
		dbSave("logs",$logData);

		$jsonReturn["status"]="ok";
		$jsonReturn["message"]="Dados editados com sucesso";

		echo newJSON($jsonReturn);
		die;
	}
}
?>