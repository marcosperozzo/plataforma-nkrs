<?php
class products_Controller extends common_Controller {

	function index(){
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Categories.php";
		$st_categories = new ST_Categories;
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Products.php";
		$st_products = new ST_Products;
		$category = null;

		if(isset($_GET['categoria'])){
			$category = $_GET['categoria'];
		}

		$categories = json_decode($st_categories->get());
		if(!empty($category)){
			$products = json_decode($st_products->get(array('category' => $category)));
			$data['page_name'] = "Categoria - ";
		}
		else {
			$products = json_decode($st_products->get());
			$data['page_name'] = "Loja";
		}
		
		foreach($products->data as $key => $item){
			$products->data[$key]->image = str_replace('../','',$item->image);
		}

		$data["title"] = "Loja - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";
		$data['categories'] = $categories->data;
		$data['products'] = $products->data;

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/store/index","layout/_footer"),$data);
	}

	function details(){
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Products.php";
		$st_products = new ST_Products;

		$product = json_decode($st_products->get(array('slug'=>$_GET['produto'], "details" => TRUE)));
		$products = json_decode($st_products->get(array('limit'=> 4)));

		foreach($product->gallery as $key => $item){
			$product->gallery[$key]->path = str_replace('../','',$item->path);
		}

		foreach($products->data as $key => $item){
			$products->data[$key]->image = str_replace('../','',$item->image);
		}

		$data["title"] = "Loja - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";
		$data['page_name'] = $product->data->title;
		$data['product'] = $product->data;
		$data['products'] = $products->data;
		$data['jsApp'] = array('cart.js');

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		//die(var_dump($data['product']));
		
		loadView(array("layout/_header","pages/store/details","layout/_footer"),$data);
	}
}
?>
