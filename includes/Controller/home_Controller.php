<?php
class home_Controller extends common_Controller {

	function index() {
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Blog.php";
		$blog = new ST_Blog;

		$response = json_decode($blog->get(array('limit' => 5)));
			
		if($response->status == true){
			foreach($response->data as $key => $post){
				$response->data[$key]->image = str_replace('/st/','/adsites/',$post->image);
			}
			$data['posts'] = $response->data;
		}
		
		// busca a pagina para preencher SEO
		$data["title"] = "Home - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";

		$data['jsApp']=array('home.js');

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		//Model to catch the interface content - You can edit this line
		// $data["content"]=$modelGeral->listaItens("interface",0,999999999,false,"no");
		loadView(array("layout/_header","pages/index","layout/_footer"),$data);
	}

	function sendEmail() {
		postRequest();
		$json=$_POST;

		//Check if the the fields aren't empty
		if (empty($json["name"]) or empty($json["email"]) or empty($json["content"])) {
			$jsonReturn["status"]="error";
			$jsonReturn["message"]="Você precisa preencher seu nome, email e mensagem.";
		} else {
			$emailData["name"]=$json["name"];
			$emailData["email"]=$json["email"];
			$emailData["phone"]=$json["phone"];
			$emailData["content"]=nl2br($json["content"]);
			dbSave('contacts',$emailData);

			$emailDataContent["name"]["label"]="Nome";
			$emailDataContent["name"]["value"]=$json["name"];
			$emailDataContent["email"]["label"]="E-mail";
			$emailDataContent["email"]["value"]=$json["email"];
			$emailDataContent["phone"]["label"]="Telefone";
			$emailDataContent["phone"]["value"]=$json["phone"];
			$emailDataContent["content"]["label"]="Mensagem";
			$emailDataContent["content"]["value"]=nl2br($json["content"]);
			$emailSubject="Mensagem enviada através do site";
			$emailIntroText="Olá. Você recebeu um e-mail enviado através do formulário de contato do site.";

			//Send the e-mail
			if (sendEmail($this->email_site,false,$emailDataContent,$emailIntroText,$emailSubject)) {
				$jsonReturn["status"]=true;
				$jsonReturn["message"]="Mensagem enviada com sucesso.";
			} else {
				$jsonReturn["status"]=false;
				$jsonReturn["message"]="Ocorreu um problema no envio do e-mail. Tente novamente mais tarde.";
			}
		}

		echo newJSON($jsonReturn);
		die;
	}
}
?>
