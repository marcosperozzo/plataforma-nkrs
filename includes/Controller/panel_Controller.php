<?php

use ___PHPSTORM_HELPERS\object;

class panel_Controller extends common_Controller {

	function index()
	{
		session_start();
		// busca a pagina para preencher SEO
		$data["title"] = "Painel do usuário - ".$this->nome_site;
		$data['content'] = "includes/View/pages/user/index.php";

		$data['client'] = (object)$_SESSION['nkrs']['client'];
		$data['jsApp'] = array("user.js");
		
		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function editUser()
	{
		session_start();
		//$modelSite=new site_Model();

		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Client.php";
		$storetrooper = new ST_Client;

		$data["title"] = "Editar usuário - ".$this->nome_site;
		$data["content"] = "includes/View/pages/user/form.php";
		$data["page_name"] = "Editar";
		$data['jsApp'] = array("user.js");
		
		$response = json_decode($storetrooper->get(true));
		$data["data"] = $response->data;

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function editPassword()
	{
		$data["title"] = "Editar senha - ".$this->nome_site;
		$data['content'] = "includes/View/pages/auth/change.php";
		$data["page_name"] = "Trocar senha";
		$data['jsApp'] = array("user.js");

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function address(){
		session_start();
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Addresses.php";
		$storetrooper = new ST_Addresses;
		// busca a pagina para preencher SEO
		$data["title"] = "Seus endereços - ".$this->nome_site;
		$data['content'] = "includes/View/pages/address/list.php";
		$data['jsApp'] = array("user.js","address.js");

		$data["page_name"] = "Seus endereços";
		
		$response = json_decode($storetrooper->get(array('client' => $_SESSION['nkrs']['client']['id'])));
		$data['data'] = $response->data;
		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function crudAddress() {
		session_start();
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Utils.php";
		$storetrooper = new ST_Utils;

		// busca os estados para poder preencher as cidades
		$response = json_decode($storetrooper->get_states());

		if($response->status == true){
			$data["states"] = $response->data;
		}

		$data["title"] = "";
		$data['content'] = "includes/View/pages/address/form.php";
		$data["page_name"] = "";
		$data['jsApp'] = array("user.js","address.js");

		if(isset($_GET["id"])){
			require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Addresses.php";
			$address = new ST_Addresses;

			$data["id"] = $_GET["id"];
			$response = json_decode($address->get(array('client' => $_SESSION['nkrs']['client']['id'], 'id' => $data["id"])));
			$data['data'] = $response->data;

			$data["title"] = "Editar Endereço - ".$this->nome_site;
			$data["page_name"] = "Editar Endereço";
		} else {
			$data["title"] = "Novo Endereço - ".$this->nome_site;
			$data["page_name"] = "Novo Endereço";
		}

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function results()
	{
		$modelSite=new site_Model();

		// busca a pagina para preencher SEO
		$data["data"] = $modelSite->getAllResults();
		$data["title"] = "Resultados - ".$this->nome_site;
		$data["metaDescription"] = "Resultados do campeonato - NKRS";
		$data["content"] = "includes/View/pages/webcup/results.php";
		$data['jsApp'] = array("user.js");

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function webcup()
	{
		$modelSite=new site_Model();

		// busca a pagina para preencher SEO
		$data["title"] = "Cadastro Webcup - ".$this->nome_site;
		$data['content'] = "includes/View/pages/webcup/form.php";

		$data["page_name"] = "Cadastro do Webcup";
		$data['jsApp'] = array("user.js");

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function editWebcup()
	{
		$modelSite = new site_Model();
		$data['jsApp'] = array("user.js");

		if(isset($_GET["id"])){
			$id = $_GET["id"];
			// busca a pagina para preencher SEO
			$data["title"] = "Editar Webcup - ".$this->nome_site;
			$data['content'] = "includes/View/pages/webcup/form.php";
	
			$data["data"] = $modelSite->getResultsById($id);	
			$data["page_name"] = "Edição do Webcup";

			//General data from the site
			$data["footerData"]=$this->footerData;
			$data["headerData"]=$this->headerData;
			$data["generalSiteData"]=$this->generalSiteData;
		}

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function crowdfounding()
	{
		$modelSite=new site_Model();

		$data["title"] = "Crowdfounding - ".$this->nome_site;
		$data['content'] = "includes/View/pages/crowdfounding/list.php";

		$data["page_name"] = "Ações de Crowdfounding - NKRS";
		$data["data"] = $modelSite->getAllCrowdfounding();
		$data["jsApp"] = array("crowdfounding.js", "user.js");
		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}

	function crudCrowdfounding()
	{
		$moduleName = "crowdfounding";
		$modelSite = new site_Model();
		$id = null;

		// busca a pagina para preencher SEO
		$data["title"] = "Gerenciar Crowdfounding - ".$this->nome_site;
		$data['content'] = "includes/View/pages/crowdfounding/form.php";
		$data["page_name"] = "Gerenciar Crowdfounding";

		if(isset($_GET["id"])){
			$id = $_GET["id"];
			$data["data"] = $modelSite->getCrowdfoundingById($id);
		}

		$data['cssVendor'] = array("jquery-fileupload/dist/css/jquery.fileupload.css", "jquery-fileupload/dist/css/jquery.fileupload-ui.css");
		$data['jsVendor'] = array("jquery-fileupload/dist/js/vendor/jquery.ui.widget.js", "jquery-fileupload/dist/js/jquery.iframe-transport.js", "jquery-fileupload/dist/js/jquery.fileupload.js");
		$data['jsApp'] = array("fileupload.js", "user.js");

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/panel/index","layout/_footer"),$data);
	}
}
?>
