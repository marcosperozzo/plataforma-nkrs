<?php
use Firebase\JWT\JWT;

class auth_Controller extends common_Controller {

	function login ($error = false) {
		$data["title"]="Entrar - ".$this->nome_site;
		$data["message"]=$error;
		loadView(array("layout/_header","pages/auth/login","layout/_footer"),$data);
	}

	function forgot($error = false) {
		$data["title"]="Esqueceu a senha? - ".$this->nome_site;
		$data["message"]=$error;
        loadView(array("layout/_header","pages/auth/recovery","layout/_footer"), $data);
	}
	
	function reset ($error = false) {
		$data["title"]="Redefinir sua senha - ".$this->nome_site;
		$data["message"]=$error;
		loadView(array("layout/_header","pages/auth/reset","layout/_footer"), $data);
	}

	function doLogin() {
		postRequest();
		
		//$modelGeral = new geral_Model();

		if(!empty($_POST)){
			require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Client.php";
			$storetrooper = new ST_Client;
			
			$data['email'] 	  = $_POST['email'];
			$data['password'] = $_POST['password'];

			$response = json_decode($storetrooper->login($data));

			
			if($response->status == true){
				$data['login'] = true;
				$data['url'] = $this->endereco_site."painel";

			//	die(var_dump($response));
			}
			
			$response->data = $data;		
		} else {
			$response['status'] = false;
			$response['message'] = "Houve um erro! Tente novamente mais tarde.";
		}

		echo json_encode($response);
	}

	function logout(){
		postRequest();

		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Client.php";
		$storetrooper = new ST_Client;
		$storetrooper->logout();
		session_destroy();
		
		$response['status'] = true;
		$response['message'] = "Sua sessão foi encerrada.";
		$response['url'] = ENDERECO_SITE;

		echo json_encode($response);
	}

	// function doLogin() {
	// 	postRequest();
	// 	limpaPost();
	// 	$endereco_site=$this->endereco_site;

	// 	$modelUser = new user_Model();
	// 	$userData = $modelUser->getUserData($_POST["username"]);

	// 	if ($userData and password_verify($_POST["password"], $userData->password)) {
	// 	    $now = new DateTime();
	// 	    $future = new DateTime("now +2 hours");

	// 	    $payload = [
	// 	        "iat" => $now->getTimeStamp(),
	// 	        "exp" => $future->getTimeStamp(),
	// 	        "uid" => $userData->id,
	// 	        "relation" => $userData->empresas_id,
	// 	        "role" => $userData->role
	// 	    ];

	// 	    $secret = base64_decode(SECRET_PHRASE);
	// 	    $token = JWT::encode($payload, $secret, "HS256");

	// 		$userData->token=$token;
	// 		newSession(false, $userData);
	// 		header("Location: ".$endereco_site."adm/home");
	// 	} else {
	// 		$this->login("Usuário ou senha inválidos");
	// 	}
	// }

	//On the forgot password view, has an ajax that call this method
	function lostPassword() {
		postRequest();

		$modelUser=new user_Model();
		$userData=$modelUser->getUserData($_POST["username"]);

		if ($userData) {
			$hash = password_hash($userData->id.$userData->role.$userData->email, PASSWORD_DEFAULT);
			$jsonData['message'] = "Hash gerada com sucesso";

			$emailData["link"]["label"]="Link";
			$emailData["link"]["value"]="<a href='".$this->endereco_site."auth/redefinir-senha/?hash=".$hash."'>Clique aqui para alterar sua senha</a>";
			$emailData["email"]["label"]="E-mail";
			$emailData["email"]["value"]=$userData->email;
			$emailData["name"]["label"]="Nome";
			$emailData["name"]["value"]=$userData->nome;
			$introText="Olá. Acesso o link abaixo para conseguir definir uma nova senha para o seu usuário.";
			$subject="Redefinição de sua senha";

			if (sendEmail($_POST["username"],false,$emailData,$introText,$subject)) {
				$jsonData["status"]="ok";
				$jsonData["message"]="E-mail enviado com sucesso. Acesse seu e-mail para trocar sua senha.";
			} else {
				$jsonData["status"]="error";
				$jsonData["message"]="Ocorreu um problema para enviar um e-mail de recuperação de senha. Tente novamente mais tarde.";
			}
		} else {
			$jsonData["status"]="error";
			$jsonData["message"]="Usuário não encontrado";
		}

		echo newJSON($jsonData);
		die;
	}

	//On the recovery password view, an Ajax call this method
	function savePassword() {
		postRequest();

		$modelUser=new user_Model();
		$userData=$modelUser->getUserData($_POST["username"]);

		if (password_verify($userData->id.$userData->role.$userData->email, $_POST["hash"])) {
			if (!empty($_POST["password"])) {
				$edit_data["id"]=$userData->id;
				$edit_data["password"]=password_hash($_POST["password"], PASSWORD_DEFAULT);
				$update=dbUpdate('users',$edit_data);

				$jsonData["status"]="ok";
				$jsonData["message"]="Senha alterada com sucesso";
			} else {
				$jsonData["status"]="error";
				$jsonData["message"]="Dados inválidos";
			}
		} else {
			$jsonData["status"]="error";
			$jsonData["message"]="Usuário sem permissão";
		}

		echo newJSON($jsonData);
		die;
	}
}
?>